import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DataProvider } from '../providers/data/data.provider';
import { SettingsProvider } from '../providers/settings/settings';
import { LogsPage } from '../pages/logs/logs';
import { StatusComponent } from '../components/status/status';
import { ScreensPage } from '../pages/screens/screens';
import { ScreenContentPage } from '../pages/screens/screen-content/screen-content';
import { LogContentPage } from '../pages/logs/log-content/log-content';
import { YahooWeatherProvider } from '../providers/yahoo-weather/yahoo-weather.provider';
import { WeatherPlacesPage } from '../pages/screens/screen-content/weather-places/weather-places';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SettingsPage,
    LogsPage,
    ScreensPage,
    ScreenContentPage,
    StatusComponent,
    LogContentPage,
    WeatherPlacesPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SettingsPage,
    LogsPage,
    LogContentPage,
    ScreensPage,
    ScreenContentPage,
    WeatherPlacesPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    SettingsProvider,
    YahooWeatherProvider,
    InAppBrowser
  ]
})
export class AppModule {}

