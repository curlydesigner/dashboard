import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-status',
  templateUrl: 'status.html'
})
export class StatusComponent {
  @Input() isOnline: boolean;

  constructor() {
  }
}
