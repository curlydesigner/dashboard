import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data.provider';
import { WsMessage } from 'pi-dashboard-shared';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  isOnline$: Observable<boolean>;

  constructor(
    public navCtrl: NavController,
    private dataProvider: DataProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController) {
    this.isOnline$ = this.dataProvider.isOnline();
  }

  ionViewDidLoad() {
  }

  handleReboot() {
    const confirm = this.alertCtrl.create({
      title: 'Reboot',
      message: 'Do you realy want to reboot the dashboard ?',
      buttons: [
        {
          text: 'No'
        },
        {
          text: 'Yes',
          handler: () => this.apiCall(() => this.dataProvider.sendWebsocketMessage({type: WsMessage.Type.reboot}))
        }
      ]
    });
    confirm.present();
  }

  apiCall(method: () => Observable<any>) {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    method().pipe(
      catchError(error => {
        loader.dismiss();
        console.error('error while calling API.', error);
        const toast = this.toastCtrl.create({
          message: 'Error to call API',
          showCloseButton: true,
          closeButtonText: 'Ok'
        });
        toast.present();
        return Observable.throw(error);
      })
    )
      .subscribe(() => {
        loader.dismiss();
      });
  }

  handleShutdown() {
    const confirm = this.alertCtrl.create({
      title: 'Shutdown',
      message: 'Do you realy want to shutdown the dashboard ?',
      buttons: [
        {
          text: 'No'
        },
        {
          text: 'Yes',
          handler: () => this.apiCall(() => this.dataProvider.sendWebsocketMessage({type: WsMessage.Type.shutdown}))
        }
      ]
    });
    confirm.present();
  }
}
