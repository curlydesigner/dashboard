import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data.provider';
import { Observable } from 'rxjs';
import { Logger, WsMessage } from 'pi-dashboard-shared';
import * as moment from 'moment';
import { LogContentPage } from './log-content/log-content';

@Component({
  selector: 'page-logs',
  templateUrl: 'logs.html',
})
export class LogsPage {
  isOnline$: Observable<boolean>;
  logs: Logger.Log[] = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private dataProvider: DataProvider,
    public modalCtrl: ModalController
    ) {
    this.isOnline$ = this.dataProvider.isOnline();
    this.dataProvider.getLogs().subscribe(logs => {
      this.logs = logs;
      this.dataProvider
        .getWebsocketMessages(WsMessage.Type.log)
        .map((message: WsMessage) => message.data as Logger.Log)
        .subscribe(log => {
          if(this.logs.length > 1000){
            this.logs.pop();
          }
          this.logs.unshift(log);
        });
    });
  }

  ionViewDidLoad() {
  }

  formatDate(num: number) {
    return moment(num).format('mm:ss:SSS');
  }

  getColor(level: number): string {
    return level === 0 ? 'danger' : '';
  }

  openModal(log: Logger.Log) {
    let modal = this.modalCtrl.create(LogContentPage, {log});
    modal.present();
  }
}
