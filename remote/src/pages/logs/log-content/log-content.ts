import { Component } from '@angular/core';
import { NavParams, Platform, ViewController } from 'ionic-angular';
import { Logger } from 'pi-dashboard-shared';
import * as moment from 'moment';

@Component({
  selector: 'page-log-content',
  templateUrl: 'log-content.html',
})
export class LogContentPage {
  log: Logger.Log;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
    ) {
    this.log = this.params.get('log');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  formatDate(num: number) {
    return moment(num).format('DD-MMM-YYYY mm:ss:SSS');
  }

  getColor(level: number): string {
    return level === 0 ? 'danger' : '';
  }
}
