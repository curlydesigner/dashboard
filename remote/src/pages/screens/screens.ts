import { Screen } from 'pi-dashboard-shared';
import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data.provider';
import { ScreenContentPage } from './screen-content/screen-content';

@Component({
  selector: 'page-screens',
  templateUrl: 'screens.html'
})
export class ScreensPage {

  isOnline$: Observable<boolean>;
  screens$: Observable<Screen[]>;

  constructor(
    public navCtrl: NavController,
    private dataProvider: DataProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController) {
    this.isOnline$ = this.dataProvider.isOnline();
    this.screens$ = this.dataProvider.getScreens();
  }

  ionViewDidLoad() {
  }

  openModal(screen: Screen) {
    let modal = this.modalCtrl.create(ScreenContentPage, {screen});
    modal.onDidDismiss(res => {
      Object.assign(screen, res);
    });
    modal.present();
  }
}
