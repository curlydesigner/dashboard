import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { YahooWeatherPlace } from '../../../../providers/yahoo-weather/yahoo-weather-types';

@Component({
  selector: 'page-weather-places',
  templateUrl: 'weather-places.html'
})
export class WeatherPlacesPage {

  places$: Observable<YahooWeatherPlace[]>;
  findPlaces: (string) => Observable<YahooWeatherPlace[]>;

  constructor(
    params: NavParams,
    public viewCtrl: ViewController) {
      this.findPlaces = params.get('findPlaces');
    }

  ionViewDidLoad() {
  }
  
  filterPlaces(ev) {
    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.places$ = this.findPlaces(val);
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  handleSelectCity(city) {
    this.viewCtrl.dismiss(city);
  }
}
