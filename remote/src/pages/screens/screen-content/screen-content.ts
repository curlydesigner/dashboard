import { WsMessage, Screen, Utils } from 'pi-dashboard-shared';
import { Observable } from 'rxjs';
import { catchError, map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Component } from '@angular/core';
import { AlertController, LoadingController, ViewController, NavParams, ModalController, Loading } from 'ionic-angular';
import { DataProvider } from '../../../providers/data/data.provider';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { YahooWeatherProvider } from '../../../providers/yahoo-weather/yahoo-weather.provider';
import { YahooWeatherPlace } from '../../../providers/yahoo-weather/yahoo-weather-types';
import { WeatherPlacesPage } from './weather-places/weather-places';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-screen-content',
  templateUrl: 'screen-content.html'
})
export class ScreenContentPage {

  STORAGE_CONFIG_KEY = 'tmp-config';

  intervals = [
    { text: '1 Minute', value: 60 * 1000 },
    { text: '2 Minutes', value: 2 * 60 * 1000 },
    { text: '5 Minutes', value: 5 * 60 * 1000 },
    { text: '10 Minutes', value: 10 * 60 * 1000 },
    { text: '15 Minutes', value: 15 * 60 * 1000 },
  ];

  isOnline$: Observable<boolean>;
  formGroup: FormGroup;
  loading = true;
  accessToken: string;
  dropBoxUrl$: Observable<string>;
  places$: Observable<YahooWeatherPlace[]>;

  constructor(
    params: NavParams,
    private dataProvider: DataProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private yahooWeatherProvider: YahooWeatherProvider,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public inAppBrowser: InAppBrowser) {

    const screen = params.get('screen');

    this.isOnline$ = this.dataProvider.isOnline();
    this.formGroup = this.formBuilder.group({
      screen: [screen && screen._id ? screen._id : '0'],
      screenName: [screen ? screen.name : null, Validators.required],
      backgroundUpdate: [screen ? screen.backgroundUpdate : null, Validators.required],
      accessToken: [this.accessToken ? this.accessToken : (screen && screen.hasAccessToken ? 'HasToken' : null), Validators.required],
      city: [screen ? { text: screen.cityId, id: screen.cityId } : null, Validators.required],
      units: [screen ? screen.units : null, Validators.required],
    });
    this.dropBoxUrl$ = this.dataProvider.getDropBoxAuthenticationUrl();
    this.places$ = this.formGroup.controls['city']
      .valueChanges
      .pipe(
        map(value => !value || typeof value === 'string' ? value : value.text),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(query => this.yahooWeatherProvider.findPlaces(query))
      );
  }

  ionViewDidLoad() {
  }

  openWeatherPlacesModal() {
    let modal = this.modalCtrl.create(WeatherPlacesPage, { findPlaces: this.yahooWeatherProvider.findPlaces.bind(this.yahooWeatherProvider) });
    modal.onDidDismiss(city => {
      this.formGroup.patchValue({ city });
    });
    modal.present();
  }

  handleConnectDropboxChange($event) {
    console.log($event);
    if (!$event.checked) {
      this.formGroup.patchValue({ accessToken: undefined });
    }
  }


  handleReload() {
    const confirm = this.alertCtrl.create({
      title: 'Reload',
      message: 'Do you realy want to reload the screen ?',
      buttons: [
        { text: 'No' },
        {
          text: 'Yes',
          handler: () => this.apiCall(() => this.dataProvider.sendWebsocketMessage({
            type: WsMessage.Type.reload,
            data: this.formGroup.controls['screen'].value
          }))
        }
      ]
    });
    confirm.present();
  }

  handleSaveScreen() {
    if (this.formGroup.valid) {
      const loader = this.presentLoading();
      localStorage.removeItem(this.STORAGE_CONFIG_KEY);
      this.dataProvider
        .saveScreen(this.getScreen())
        .catch((error) => {
          loader.dismiss();
          return Observable.throw(error);
        })
        .subscribe((screen) => {
          loader.dismiss();
          this.viewCtrl.dismiss(screen);
        });
    }
  }

  getScreen(): Screen {
    const screenId = this.formGroup.controls['screen'].value;
    return {
      _id: screenId === '0' ? undefined : screenId,
      name: this.formGroup.controls['screenName'].value,
      cityId: this.formGroup.controls['city'].value.id,
      units: this.formGroup.controls['units'].value,
      backgroundUpdate: this.formGroup.controls['backgroundUpdate'].value,
      accessToken: this.formGroup.controls['accessToken'].value === 'HasToken'
        ? undefined
        : this.formGroup.controls['accessToken'].value,
    } as Screen;
  }

  handleRemoveScreen() {
    const confirm = this.alertCtrl.create({
      title: 'Remove',
      message: 'Do you realy want to remove the screen ?',
      buttons: [
        { text: 'No' },
        {
          text: 'Yes',
          handler: this.removeScreen.bind(this)
        }
      ]
    });
    confirm.present();
  }

  removeScreen() {
    const loader = this.presentLoading();
    localStorage.removeItem(this.STORAGE_CONFIG_KEY);
    this.dataProvider
      .removeScreen(this.formGroup.controls['screen'].value)
      .catch((error) => {
        loader.dismiss();
        return Observable.throw(error);
      })
      .subscribe(() => {
        loader.dismiss();
        this.viewCtrl.dismiss();
      });
  }

  private apiCall(method: () => Observable<any>) {
    const loader = this.presentLoading();

    method().pipe(
      catchError(error => {
        loader.dismiss();
        console.error('error while calling API.', error);
        // const toast = this.toastCtrl.create({
        //   message: 'Error to call API',
        //   showCloseButton: true,
        //   closeButtonText: 'Ok'
        // });
        // toast.present();
        return Observable.throw(error);
      })
    )
      .subscribe(() => {
        loader.dismiss();
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentLoading(): Loading {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    return loader;
  }

  dropboxLogin() {
    this.dataProvider
      .getDropBoxAuthenticationUrl()
      .flatMap(url => Observable.fromPromise(
        new Promise((resolve, reject) => {
          const browser = this.inAppBrowser.create(url, '_blank');
          const listener = browser
            .on('loadstart')
            .subscribe((event: any) => {
              //Ignore the dropbox authorize screen
              if (event.url.indexOf('oauth2/authorize') > -1) {
                return;
              }

              //Check the redirect uri
              if (event.url.indexOf('http://localhost') > -1) {
                listener.unsubscribe();
                browser.close();
                const params = Utils.parseQuery(event.url);
                const token = params['access_token'];
                if (token) {
                  resolve(token);
                } else {
                  reject('Could not authenticate, token not found in url params');
                }
              } else {
                reject('Could not authenticate');
              }
            });
        })
      ))
      .subscribe((accessToken: string) => {
        this.formGroup.patchValue({ accessToken });
      })
  }
}
