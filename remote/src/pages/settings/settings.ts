import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { SettingsProvider } from '../../providers/settings/settings';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  formGroup: FormGroup;
  loading = false;
  error: string;
  warning: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private settingsProvider: SettingsProvider) {
    this.formGroup = new FormGroup({
      serverURL: new FormControl(settingsProvider.baseURI, Validators.required),
    });
  }

  handleSave() {
    if (this.formGroup.valid) {
      this.settingsProvider.setSettings({
        baseURI: this.formGroup.controls['serverURL'].value
      });
      this.navCtrl.setRoot(HomePage);
    }
  }

  handleDetectServer() {
    this.loading = true;
    this.warning = null;
    this.settingsProvider
      .detectServer()
      .subscribe(apiUrl => {
        this.loading = false;
        this.formGroup.patchValue({ serverURL: apiUrl });
      },
        () => this.loading = false,
        () => {
          if (!this.formGroup.controls['serverURL'].value) {
            this.warning = 'The server was not found on local network.';
          }
          this.loading = false;
        });
  }
}
