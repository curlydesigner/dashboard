

export interface YahooWeatherResponse {
    query: {
        results: {
            channel: YahooWeatherData
        }
    };
}

export interface YahooWeatherData {
    units: {
        distance: string,
        pressure: string,
        speed: string,
        temperature: string
    };
    title: string;
    description: string;
    location: {
        city: string,
        country: string,
        region: string
    };
    wind: {
        chill: number,
        direction: number,
        directionName?: string
        speed: number,
        beaufort?: Beaufort
    };
    atmosphere: {
        humidity: string,
        pressure: string,
        rising: string,
        visibility: string
    };
    astronomy: {
        sunrise: string,
        sunset: string
    };
    item: {
        title: string,
        lat: string,
        long: string,
        condition: {
            code: string,
            date: string,
            temp: string,
            text: string
        },
        forecast: YahooWeatherForecast[],
        description: string,
    };
}

export interface YahooWeatherForecast {
    code: string;
    date: string;
    day: string;
    high: string;
    low: string;
    text: string;
}

export interface Beaufort {
    grade: number; desc: string;
}

export type Units = 'c' | 'f';

export interface YahooWeatherPlace {
    text: string;
    id: string;
}
