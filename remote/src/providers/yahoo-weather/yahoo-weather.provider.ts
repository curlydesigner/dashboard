import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { YahooWeatherResponse, YahooWeatherData, Units, YahooWeatherPlace } from './yahoo-weather-types';
import { map } from 'rxjs/operators';
import * as beaufort from 'beaufort-scale';
import { Logger } from 'pi-dashboard-shared';

@Injectable()
export class YahooWeatherProvider {

    static BASE_URL = 'http://query.yahooapis.com/v1/public/yql';
    static WIND_DIRECTIONS = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];

    logger = new Logger('YahooWeatherProvider');

    constructor(private http: HttpClient) { }

    getWeatherData(cityId: string, units: Units): Observable<YahooWeatherData> {
        const query = encodeURIComponent('select * from weather.forecast where woeid=' + cityId + ' AND u="' + units + '"');
        const url = YahooWeatherProvider.BASE_URL + '?q=' + query + '&format=json';
        return Observable.onErrorResumeNext(
            this.http
                .get<YahooWeatherResponse>(url)
                .repeatWhen(() => Observable.interval(1000 * 15 * 60))
                .catch(error => {
                        this.logger.error(error);
                        return Observable.empty<YahooWeatherResponse>();
                    })
                .map((response) => this._processResponse(units, response))
        );
    }

    findPlaces(query: string): Observable<YahooWeatherPlace[]> {
        if (!query || query.length === 0) {
            return Observable.of([]);
        }
        const now = new Date();
        const urlQuery = encodeURIComponent('select * from geo.places where text="' + query + '"');
        const url = YahooWeatherProvider.BASE_URL +
            '?q=' +
            urlQuery +
            '&rnd=' +
            now.getFullYear() +
            now.getMonth() +
            now.getDay() +
            now.getHours() +
            '&format=json';

        return this.http.get<YahooWeatherResponse>(url).pipe(map(response => {
            let places = response.query.results ? response.query.results['place'] : [];
            places = places.length ? places : [places];
            return places.map(item => {
                const country = (item.country.content) ? item.country.content : '';
                const name = item.name;
                const admin = (item.admin1) ? item.admin1.content + ', ' : '';
                const woeid = item.woeid;
                return { text: name + ', ' + admin + country, id: woeid + '' };
            });
        }));
    }

    private _processResponse(units: Units, response: YahooWeatherResponse): YahooWeatherData {
        const results = response.query.results.channel;

        // convert wind chill from fahrenheit to celsius
        results.wind.chill = units === 'c' ? this._fahrenheitToCelsius(results.wind.chill) : results.wind.chill;
        // convert wind degree to direction name
        results.wind.directionName = this._degreeToDirection(results.wind.direction);
        // convert wind speed from km to beaufort
        results.wind.beaufort = beaufort(results.wind.speed);

        return results;
    }


    private _fahrenheitToCelsius(fahrenheit: number): number {
        return (fahrenheit - 32) * 0.5556;
    }

    private _degreeToDirection(degree: number): string {
        const idx = ((degree / 22.5) + .5).toFixed(0);
        return YahooWeatherProvider.WIND_DIRECTIONS[idx];
    }
}
