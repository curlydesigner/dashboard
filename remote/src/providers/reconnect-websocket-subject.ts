import { Subject, Observable, Observer, BehaviorSubject } from "rxjs";
import { WebSocketSubjectConfig, WebSocketSubject } from "rxjs/observable/dom/WebSocketSubject";

export class ReconnectWebsocketSubject<T> extends Subject<T> {
    private reconnectionObservable: Observable<number>;
    private wsSubjectConfig: WebSocketSubjectConfig;
    private socket: WebSocketSubject<any>;
    private connectionObserver: Observer<boolean>;
    public connectionStatus: Observable<boolean>;
    public isConnected: BehaviorSubject<boolean> = new BehaviorSubject(false);

    // defaultResultSelector = (e: MessageEvent) => {
    //     return JSON.parse(e.data);
    // }

    defaultSerializer = (data: any): string => {
        return JSON.stringify(data);
    }

    constructor(
        url: string,
        private reconnectInterval: number = 5000,
        // private reconnectAttempts: number = 10,
        // private resultSelector?: (e: MessageEvent) => any,
        private serializer?: (data: any) => string,
    ) {
        super();

        this.connectionStatus = new Observable<boolean>((observer) => {
            this.connectionObserver = observer;
        }).share().distinctUntilChanged();

        // if (!resultSelector) {
        //     this.resultSelector = this.defaultResultSelector;
        // }
        if (!this.serializer) {
            this.serializer = this.defaultSerializer;
        }

        this.wsSubjectConfig = {
            url: url,
            closeObserver: {
                next: (e: CloseEvent) => {
                    this.socket = null;
                    this.connectionObserver.next(false);
                }
            },
            openObserver: {
                next: (e: Event) => {
                    this.connectionObserver.next(true);
                }
            }
        };
        this.connect();
        this.connectionStatus.subscribe((isConnected) => {
            this.isConnected.next(isConnected);
            if (!this.reconnectionObservable && typeof (isConnected) == "boolean" && !isConnected) {
                this.reconnect();
            }
        });
    }

    resetUrl(url: string): any {
        this.wsSubjectConfig.url = url;
        if (this.socket) {
            this.socket.complete();
            this.socket.unsubscribe();
        }
        this.connect();
    }

    connect(): void {
        this.socket = new WebSocketSubject(this.wsSubjectConfig);
        this.socket.subscribe(
            (message) => this.next(message),
            (error: Event) => {
                if (!this.socket) {
                    this.reconnect();
                }
            });
    }

    reconnect(): void {
        this.reconnectionObservable = Observable.interval(this.reconnectInterval)
            .takeWhile((v, index) => {
                // return index < this.reconnectAttempts && !this.socket
                return !this.socket
            });
        this.reconnectionObservable.subscribe(
            () => {
                this.connect();
            },
            null,
            () => {
                this.reconnectionObservable = null;
                if (!this.socket) {
                    this.complete();
                    this.connectionObserver.complete();
                }
            });
    }

    send(data: T): void {
        this.socket.next(this.serializer(data));
    }
}

