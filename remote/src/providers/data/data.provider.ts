import { WsMessage, Screen, Logger, WsClientType } from 'pi-dashboard-shared';
import { SettingsProvider } from '../settings/settings';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReconnectWebsocketSubject } from '../reconnect-websocket-subject';

@Injectable()
export class DataProvider {
  private logger = new Logger('DataProvider');
  private socket$: ReconnectWebsocketSubject<WsMessage>;

  constructor(public http: HttpClient, private settingsProvider: SettingsProvider) {
    settingsProvider.baseURI$.subscribe(this.initWsConnection.bind(this));
  }

  initWsConnection(baseURI: string) {
    this.logger.debug('initWsConnection', baseURI);
    //if(baseURI) {
      const url = baseURI.replace('http://', 'ws://') + `/ws?type=${WsClientType.remote}`;
      if(this.socket$) {
        this.socket$.resetUrl(url);
      } else {
        this.socket$ = new ReconnectWebsocketSubject<WsMessage>(url)
        this.getWebsocketMessages(WsMessage.Type.ping)
        .subscribe(() => {
          this.logger.debug('ping');
          this.socket$.send({
            type: WsMessage.Type.pong
          });
        })
      }
    //}
  }

  getWebsocketMessages(type: WsMessage.Type) {
    return this.socket$.filter(message => message.type === type);
  }

  sendWebsocketMessage(message: WsMessage): Observable<WsMessage> {
    this.socket$.send(message);
    return Observable.of(message);
  }

  getLogs(): Observable<Logger.Log[]> {
    const endpoint = this.settingsProvider.baseURI + '/admin/logs';
    return this.http.get<Logger.Log[]>(endpoint);
  }

  isOnline(): Observable<boolean> {
    return this.socket$.isConnected;
  }

  getScreens(): Observable<Screen[]> {
    const endpoint = this.settingsProvider.baseURI + '/screens';
    return this.http.get<Screen[]>(endpoint);
  }

  saveScreen(screen: Screen): Observable<Screen> {
    return this.http.put<Screen>(this.settingsProvider.baseURI + '/screens/' + screen._id, screen);
  }

  removeScreen(id: string) {
    return this.http.delete(this.settingsProvider.baseURI + '/screens/' + id);
  }

  getDropBoxAuthenticationUrl(): Observable<string> {
    return this.http.get<string>(this.settingsProvider.baseURI + '/dropbox-authentication-url');
  }
}
