import { Injectable } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';
import { Logger } from 'pi-dashboard-shared';
import { UdpClientProvider } from '../udp-client.provider';

export interface Settings {
  baseURI: string;
}

@Injectable()
export class SettingsProvider {

  static STORAGE_KEY = 'settings';

  logger = new Logger('SettingsProvider');

  private settings: Settings = {} as Settings;

  baseURI$ = new ReplaySubject<string>();

  get baseURI(): string {
    return this.settings.baseURI;
  }

  constructor() {
    const settingsStr = localStorage.getItem(SettingsProvider.STORAGE_KEY);
    if(settingsStr) {
      this.setSettings(JSON.parse(settingsStr) as Settings);
    }
  }

  setSettings(settings: Settings) {
    this.logger.debug('setSettings', settings);
    this.settings = settings;
    localStorage.setItem(SettingsProvider.STORAGE_KEY, JSON.stringify(settings))
    this.baseURI$.next(this.baseURI);
  }

  detectServer(): Observable<string> {
    return new UdpClientProvider().sendUDPMessage('-remote');
  }
}
