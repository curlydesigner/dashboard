import { ReplaySubject, Observable } from "rxjs";
import { Logger, UdpConfig, Utils } from "pi-dashboard-shared";

declare var chrome;

export class UdpClientProvider {
    private socketId: number;
    private udpStream: ReplaySubject<string> = new ReplaySubject();
    private logger = new Logger('UdpClientProvider');

    constructor(
        private ip: string = '0.0.0.0',
        private ttl: number = 128,
        private timetolisten: number = 5000) { }

    sendUDPMessage(message: string): Observable<string> {
        // only do udp stuff if there is plugin defined
        if (typeof chrome.sockets !== 'undefined') {
            // register the listeners
            chrome.sockets.udp.onReceive.addListener(this.onReceive.bind(this));
            chrome.sockets.udp.onReceiveError.addListener((error) => {
                this.logger.error('Received ERROR from socket', error);
                this.udpStream.error(error);
            });
            // translate the string into ArrayBuffer
            const buffer = Utils.str2ab(UdpConfig.prefix + message);
            // send  the UDP search as captures in UPNPSTRING and to port PORT
            chrome.sockets.udp.create(this.onCreate.bind(this, buffer));
            // and close the listener after a while
            setTimeout(() => {
                this.closeUDPService();
            }, this.timetolisten);
        }
        // return the stream
        return this.udpStream.asObservable();
    }

    closeUDPService() {
        // close the socket
        if (typeof chrome.sockets !== 'undefined') {
            chrome.sockets.udp.close(this.socketId);
        }
        // close the stream
        this.udpStream.complete();
    }

    private onReceive(info) {
        const dataStr = Utils.ab2str(info.data) //to be written - many examples out there.
        this.udpStream.next(dataStr);
    }

    private onCreate(buffer: ArrayBuffer, createInfo) {
        this.socketId = createInfo.socketId;
        chrome.sockets.udp.bind(this.socketId, this.ip, 0, this.onBind.bind(this, buffer));
    }

    private onBind(buffer: ArrayBuffer) {
        chrome.sockets.udp.setMulticastTimeToLive(this.socketId, this.ttl, this.onSetMulticastTimeToLive.bind(this, buffer));
    }

    private onSetMulticastTimeToLive(buffer: ArrayBuffer) {
        chrome.sockets.udp.setBroadcast(this.socketId, true, this.onSetBroadcast.bind(this, buffer));
    }

    private onSetBroadcast(buffer: ArrayBuffer) {
        chrome.sockets.udp.send(this.socketId, buffer, UdpConfig.membership, UdpConfig.port, this.onSend.bind(this));
    }

    private onSend(sendResult) {
        if (sendResult < 0) {
            this.logger.error('Send fail', sendResult);
            // close all the stuff, send has failed
            //this.closeUDPService();
            this.udpStream.error(sendResult);
        } else {
            this.logger.debug('sendTo: success', UdpConfig.port, sendResult);
        }
    }
}
