import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DarkSkyConfigurationComponent } from './dark-sky-configuration.component';

describe('PiDashboardWeatherDarkSkyComponent', () => {
  let component: DarkSkyConfigurationComponent;
  let fixture: ComponentFixture<DarkSkyConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DarkSkyConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DarkSkyConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
