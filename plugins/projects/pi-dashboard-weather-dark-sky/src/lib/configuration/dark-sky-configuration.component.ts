import { Component, Input, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Weather, Logger } from 'pi-dashboard-shared';
import { Configuration } from '../model/configuration';
import { Observable, NEVER } from 'rxjs';
import { GeoCode } from '../model/geocode';
import { map, debounceTime, distinctUntilChanged, switchMap, catchError, tap } from 'rxjs/operators';
import { DarkSkyService } from '../dark-sky.service';
import { HttpClient } from '@angular/common/http';

@Component({
    styleUrls: ['dark-sky-configuration.component.css'],
    selector: 'lib-pi-dashboard-weather-dark-sky',
    templateUrl: 'dark-sky-configuration.component.html',
    styles: []
})
export class DarkSkyConfigurationComponent implements Weather.ConfigurationComponent, OnChanges {
    @Input() config: Configuration;

    formGroup: FormGroup;
    locations$: Observable<GeoCode[]>;
    logger = new Logger('DarkSkyConfigurationComponent');

    get valid(): boolean {
        return this.formGroup.valid;
    }

    get value(): Configuration {
        return this.formGroup.value;
    }

    constructor(private formBuilder: FormBuilder, private http: HttpClient) {
        this.formGroup = this.formBuilder.group({
            apiKey: [null, Validators.required],
            // googleApiKey: [null, Validators.required],
            // location: [null, Validators.required],
            longitude: [null, Validators.required],
            latitude: [null, Validators.required],
            units: [null, Validators.required],
        });
        // this.locations$ = this.formGroup.controls.location
        //     .valueChanges
        //     .pipe(
        //         map(value => !value || typeof value === 'string' ? value : value.name),
        //         debounceTime(500),
        //         distinctUntilChanged(),
        //         switchMap(query => DarkSkyService.geoCode(this.http, query, this.formGroup.controls.googleApiKey.value)),
        //         catchError(error => {
        //             this.logger.error('Something went wrong when looking for the address', error);
        //             this.formGroup.controls.location.setErrors({
        //                 'general': 'Oops! Something went wrong when looking for the address!'
        //             });
        //             this.formGroup.controls.location.markAsTouched();
        //             return NEVER;
        //         })
        //     );
    }

    ngOnChanges() {
        this.formGroup.patchValue({
            apiKey: this.config.apiKey,
            longitude: this.config.longitude,
            latitude: this.config.latitude,
            // googleApiKey: this.config.googleApiKey,
            // location: this.config.location,
            units: this.config.units
        });
    }

    // locationAutocompleteDisplay(location?: GeoCode): string | undefined {
    //     return location ? location.name : undefined;
    // }
}
