
export enum Units {
    auto = 'auto',
    ca = 'ca',
    uk = 'uk2',
    us = 'us',
    si = 'si'
}
