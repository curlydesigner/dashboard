
export interface Response {
    /**
     * The requested latitude.
     */
    latitude: number;
    /**
     * The requested longitude.
     */
    longitude: number;
    /**
     * The IANA timezone name for the requested location.
     * This is used for text summaries and for determining when hourly and daily data block objects begin.
     */
    timezone: string;
    /**
     * Containing the current weather conditions at the requested location.
     */
    currently: Response.Currently;
    /**
     * Containing the weather conditions day-by-day for the next week.
     */
    daily: Response.Daily;
    /**
     * An alerts array, which, if present, contains any severe weather alerts pertinent to the requested location.
     */
    alerts: Response.Alert[];
}

export namespace Response {
    export enum Icon {
        'clear-day' = 'clear-day',
        'clear-night' = 'clear-night',
        'rain' = 'rain',
        'snow' = 'snow',
        'sleet' = 'sleet',
        'wind' = 'wind',
        'fog' = 'fog',
        'cloudy' = 'cloudy',
        'partly-cloudy-day' = 'partly-cloudy-day',
        'partly-cloudy-night' = 'partly-cloudy-night'
    }

    export enum PrecipType {
        rain = 'rain',
        snow = 'snow',
        sleet = 'sleet'
    }

    export interface Data {
        /**
         * The UNIX time at which this data point begins.
         * Daily data point objects to midnight of the day, all according to the local time zone.
         */
        time: number;
        /**
         * A human-readable text summary of this data point.
         * (This property has millions of possible values, so don’t use it for automated purposes: use the icon property, instead!)
         */
        summary: string;
        /**
         * A machine-readable text summary of this data point, suitable for selecting an icon for display.
         * (Developers should ensure that a sensible default is defined, as additional values, such as hail,
         * thunderstorm, or tornado, may be defined in the future.)
         */
        icon?: Icon;
        /**
         * The intensity (in inches of liquid water per hour) of precipitation occurring at the given time.
         * This value is conditional on probability (that is, assuming any precipitation occurs at all).
         */
        precipIntensity?: number;
        /**
         * The standard deviation of the distribution of precipIntensity.
         * (We only return this property when the full distribution, and not merely the expected mean, can be estimated with accuracy.)
         */
        precipIntensityError?: number;
        /**
         * The probability of precipitation occurring, between 0 and 1, inclusive.
         */
        precipProbability?: number;
        /**
         * The type of precipitation occurring at the given time.
         * If defined, this property will have one of the following values: "rain", "snow", or "sleet"
         * (which refers to each of freezing rain, ice pellets, and “wintery mix”).
         * (If precipIntensity is zero, then this property will not be defined. Additionally,
         * due to the lack of data in our sources, historical precipType information is usually estimated, rather than observed.)
         */
        precipType: PrecipType;
        /**
         * The dew point in degrees
         */
        dewPoint: number;
        /**
         * The relative humidity, between 0 and 1, inclusive.
         */
        humidity: number;
        /**
         * The sea-level air pressure in millibars.
         */
        pressure: number;
        /**
         * The wind speed.
         */
        windSpeed?: number;
        /**
         * The wind gust speed.
         */
        windGust?: number;
        /**
         * The direction that the wind is coming from in degrees, with true north at 0° and progressing clockwise.
         * (If windSpeed is zero, then this value will not be defined.)
         */
        windBearing?: number;
        /**
         * The percentage of sky occluded by clouds, between 0 and 1, inclusive.
         */
        cloudCover: number;
        /**
         * The UV index.
         */
        uvIndex: number;
        /**
         * The average visibility in miles, capped at 10 miles.
         */
        visibility: number;
        /**
         * The columnar density of total atmospheric ozone at the given time in Dobson units.
         */
        ozone: number;
    }

    export interface Currently extends Data {
        /**
         * The approximate distance to the nearest storm in miles.
         * (A storm distance of 0 doesn’t necessarily refer to a storm at the requested location,
         * but rather a storm in the vicinity of that location.)
         */
        nearestStormDistance?: number;
        /**
         * The air temperature in degrees.
         */
        temperature: number;
        /**
         * The apparent (or “feels like”) temperature in degrees.
         */
        apparentTemperature: number;
    }

    export interface Daily {
        summary: string;
        icon: string;
        data: DailyData[];
    }

    export interface DailyData extends Data {
        sunriseTime: number;
        sunsetTime: number;
        moonPhase: number;
        temperatureHigh: number;
        temperatureHighTime: number;
        temperatureLow: number;
        temperatureLowTime: number;
        apparentTemperatureHigh: number;
        apparentTemperatureHighTime: number;
        apparentTemperatureLow: number;
        apparentTemperatureLowTime: number;
        temperatureMin: number;
        temperatureMinTime: number;
        temperatureMax: number;
        temperatureMaxTime: number;
        apparentTemperatureMin: number;
        apparentTemperatureMinTime: number;
        apparentTemperatureMax: number;
        apparentTemperatureMaxTime: number;
    }

    export interface Alert {
        title: string;
        time: number;
        expires: number;
        description: string;
        uri: string;
    }
}
