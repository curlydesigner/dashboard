import { Weather } from 'pi-dashboard-shared';
import { Units } from './units';

export interface Configuration extends Weather.Configuration {
    longitude: string;
    latitude: string;
    apiKey: string;
    googleApiKey: string;
    units: Units;
    location: string;
}
