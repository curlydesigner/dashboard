
export interface GeoCode extends GeoCode.Location {
    name: string;
}

export namespace GeoCode {
    export interface Location {
        lat: number;
        lng: number;
    }
    export interface GeoResponse {
        results: Result[];
        status: string;
        error_message?: string;
    }

    export interface Result {
        formatted_address: string;
        geometry: Geometry;
    }

    export interface Geometry {
        location: Location;
    }
}
