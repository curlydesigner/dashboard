import { NgModule } from '@angular/core';
import { DarkSkyConfigurationComponent } from './configuration/dark-sky-configuration.component';
import {
  MatRadioModule,
  MatFormFieldModule,
  MatInputModule,
  MatAutocompleteModule,
  MatSelectModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule
  ],
  declarations: [
    DarkSkyConfigurationComponent
  ],
  exports: [
    DarkSkyConfigurationComponent
  ],
  entryComponents: [
    DarkSkyConfigurationComponent
  ]
})
export class DarkSkyModule { }
