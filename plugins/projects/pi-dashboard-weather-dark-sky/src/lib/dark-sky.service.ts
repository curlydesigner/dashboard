import { Observable, interval, onErrorResumeNext, NEVER } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, repeatWhen, catchError, tap, switchMap } from 'rxjs/operators';
import { Logger, Weather } from 'pi-dashboard-shared';
import * as moment from 'moment';
import { Configuration } from './model/configuration';
import { GeoCode } from './model/geocode';
import { Response } from './model/response';

export class DarkSkyService implements Weather.Service {
  static BASE_URL = 'https://api.darksky.net/forecast/{APIKEY}/{LATITUDE},{LONGITUDE}?exclude=minutely,hourly,flags&units={UNITS}';
  static ICONS = {
    'clear-day': 'day-sunny',
    'clear-night': 'night-clear',
    'rain': 'day-rain',
    'snow': 'day-snow',
    'sleet': 'day-sleet',
    'wind': 'day-windy',
    'fog': 'day-fog',
    'cloudy': 'day-cloudy',
    'partly-cloudy-day': 'day-cloudy-high',
    'partly-cloudy-night': 'night-cloudy-high'
  };
  // static GEO_CODE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?address={ADDRESS}&key={GOOGLE-API-KEY}';

  logger = new Logger('DarkSkyService');

  constructor(private http: HttpClient, private config: Configuration) { }

  // static geoCode(http: HttpClient, address: string, key: string): Observable<GeoCode[]> {
  //   const url = DarkSkyService.GEO_CODE_URL.replace('{ADDRESS}', address).replace('{GOOGLE-API-KEY}', key);
  //   return http
  //     .get<GeoCode.GeoResponse>(url)
  //     .pipe(
  //       map(response => {
  //         if (response.status !== 'OK') {
  //           throw Error(response.error_message);
  //         }
  //         return response.results.map(result => Object.assign({},
  //           result.geometry.location,
  //           { name: result.formatted_address }
  //         ));
  //       })
  //     );
  // }
  /**
   * error_message: "The provided API key is invalid."
  results: []
  status: "REQUEST_DENIED"
   */
  getWeatherData(): Observable<Weather.Data> {
    let url = DarkSkyService.BASE_URL
      .replace('{LONGITUDE}', this.config.longitude)
      .replace('{LATITUDE}', this.config.latitude)
      .replace('{APIKEY}', this.config.apiKey)
      .replace('{UNITS}', this.config.units);

    url = `${this.config.proxyUrl}?url=${encodeURIComponent(url)}`;

    return onErrorResumeNext(
      this.http
        .get<Response>(url)
        .pipe(
          repeatWhen(() => interval(1000 * 15 * 60)),
          catchError(error => {
            this.logger.error(error);
            return NEVER;
          }),
          map(this._processResponse.bind(this))
        )
    );
  }

  private _processResponse(response: Response): Weather.Data {
    const sunset = moment.unix(response.daily.data[0].sunsetTime).valueOf();
    const weatherData: Weather.Data = {
      type: null,
      feelsLike: response.currently.apparentTemperature,
      date: moment.unix(response.currently.time).valueOf(),
      title: response.timezone,
      temp: response.currently.temperature,
      sunset,
      sunrise: moment.unix(response.daily.data[0].sunriseTime).valueOf(),
      humidity: response.currently.humidity * 100,
      conditionCode: this._getCode(response.currently.icon, sunset),
      conditionDesc: this._getConditionDescription(response.currently.icon),
      wind: {
        speed: response.currently.windSpeed,
        direction: response.currently.windBearing
      },
      forecast: response.daily.data.slice(1).map(item => {
        return {
          conditionCode: this._getCode(item.icon),
          date: moment.unix(item.time).valueOf(),
          high: item.temperatureHigh,
          low: item.temperatureLow,
          conditionDesc: this._getConditionDescription(item.icon)
        } as Weather.Forecast;
      })
    };
    return weatherData;
  }

  private _getConditionDescription(icon: Response.Icon) {
    return icon.replace(/\-/g, ' ').replace(' day', '').replace(' night', '');
  }

  private _getCode(darkSkyCode: string, sunset?: number) {
    let code = DarkSkyService.ICONS[darkSkyCode];
    if (sunset && Date.now() > sunset) {
      code = code.replace('day-', 'night-');
    }
    return code;
  }
}
