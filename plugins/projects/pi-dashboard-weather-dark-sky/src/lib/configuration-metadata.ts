import { Weather } from 'pi-dashboard-shared';
import { DarkSkyConfigurationComponent } from './configuration/dark-sky-configuration.component';

export const configurationMetadata: Weather.ConfigurationComponentMetadata = {
    component: DarkSkyConfigurationComponent,
    name: 'Dark Sky',
    type: 'darkSky'
};
