import { Component, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Weather } from 'pi-dashboard-shared';
import { Configuration } from '../model/configuration';


@Component({
    selector: 'lib-pi-dashboard-weather-dark-sky',
    templateUrl: 'open-weather-configuration.component.html',
    styleUrls: ['open-weather-configuration.component.css']
})
export class OpenWeatherConfigurationComponent implements Weather.ConfigurationComponent, OnChanges {
    @Input() config: Configuration;

    formGroup: FormGroup;

    get valid(): boolean {
        return this.formGroup.valid;
    }

    get value(): Configuration {
        return this.formGroup.value;
    }

    constructor(private formBuilder: FormBuilder) {
        this.formGroup = this.formBuilder.group({
            apiKey: [null, Validators.required],
            location: [null, Validators.required],
            units: [null, Validators.required],
        });
    }

    ngOnChanges() {
        this.formGroup.patchValue({
            apiKey: this.config.apiKey,
            location: this.config.location,
            units: this.config.units
        });
    }
}
