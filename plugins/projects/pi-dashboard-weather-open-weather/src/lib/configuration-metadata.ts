import { OpenWeatherConfigurationComponent } from './configuration/open-weather-configuration.component';
import { Weather } from 'pi-dashboard-shared';


export const configurationMetadata: Weather.ConfigurationComponentMetadata = {
    component: OpenWeatherConfigurationComponent,
    name: 'Open Weather',
    type: 'openWeather'
};
