import { NgModule } from '@angular/core';
import { OpenWeatherConfigurationComponent } from './configuration/open-weather-configuration.component';
import {
  MatRadioModule,
  MatFormFieldModule,
  MatInputModule,
  MatAutocompleteModule,
  MatSelectModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule
  ],
  declarations: [
    OpenWeatherConfigurationComponent
  ],
  exports: [
    OpenWeatherConfigurationComponent
  ],
  entryComponents: [
    OpenWeatherConfigurationComponent
  ]
})
export class OpenWeatherModule { }
