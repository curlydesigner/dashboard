import { Observable, interval, onErrorResumeNext, NEVER, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, repeatWhen, catchError, flatMap } from 'rxjs/operators';
import { Logger, Weather } from 'pi-dashboard-shared';
import * as Feels from 'feels';
import * as moment from 'moment';
import { Request } from './model/request';
import { Response } from './model/response';
import { Configuration } from './model/configuration';
import { Units } from './model/units';

interface Response {
  weather: Response.WeatherResponse;
  forecast: Response.ForecastResponse;
}

export class OpenWeatherService implements Weather.Service {

  static BASE_URL = 'http://api.openweathermap.org/data/2.5/{REQUEST}?q={CITY-ID}&units={UNITS}&appid={APIKEY}';

  logger = new Logger('OpenWeatherService');

  constructor(private http: HttpClient, private config: Configuration) { }

  getWeatherData(): Observable<Weather.Data> {
    const url = OpenWeatherService.BASE_URL
      .replace('{CITY-ID}', this.config.location)
      .replace('{UNITS}', Units[this.config.units])
      .replace('{APIKEY}', this.config.apiKey);
    return onErrorResumeNext(
      this.http
        .get<Response.WeatherResponse>(url.replace('{REQUEST}', Request.weather))
        .pipe(
          repeatWhen(() => interval(1000 * 15 * 60)),
          catchError(error => {
            this.logger.error(error);
            return NEVER;
          }),
          flatMap(weather => {
            return this.http
              .get<Response.ForecastResponse>(url.replace('{REQUEST}', Request.forecast))
              .pipe(
                map(forecast => ({ weather, forecast })),
              );
          }),
          map((response: Response) => {
            return this._processResponse(response);
          }),
        )
    );
  }

  private _processResponse({ weather, forecast }: Response): Weather.Data {
    const feelsLike = new Feels({
      temp: weather.main.temp,
      humidity: weather.main.humidity,
      speed: weather.wind.speed,
      units: {
        temp: 'c',
        speed: 'kph'
      }
    }).like();
    const weatherData: Weather.Data = {
      type: 'owm',
      feelsLike,
      date: moment.unix(weather.dt).valueOf(),
      title: `${weather.name}, ${weather.sys.country}`,
      temp: weather.main.temp,
      sunset: moment.unix(weather.sys.sunset).valueOf(),
      sunrise: moment.unix(weather.sys.sunrise).valueOf(),
      humidity: weather.main.humidity,
      conditionCode: `${weather.weather[0].id}`,
      conditionDesc: weather.weather[0].description,
      wind: {
        speed: weather.wind.speed,
        direction: weather.wind.deg
      },
      forecast: this._processForecast(forecast.list)
    };
    return weatherData;
  }

  private _processForecast(list: Response.ForecastItem[]): Weather.Forecast[] {
    const daily = list.reduce((acc, item) => {
      const key = item.dt_txt.substring(0, 10);
      acc[key] = acc[key] || [];
      acc[key].push(item);
      return acc;
    }, {});
    return Object.keys(daily).map(key => {
      return this._hourlyToDaily(daily[key]);
    }).slice(1);
  }

  private _hourlyToDaily(list: Response.ForecastItem[]): Weather.Forecast {
    let dailyHigh = -999;
    let dailyLow = 999;
    let sumTemp = 0;
    let sumHumidity = 0;
    let sumWindSpeed = 0;
    let sumRainVolume = 0;
    let sumSnowVolume = 0;
    const weatherDescriptions: {
      [id: number]: {
        count: number;
        id: number;
        description: string;
      }
    } = {};

    for (const item of list) {
      sumTemp += item.main.temp;
      dailyHigh = item.main.temp_max > dailyHigh ? item.main.temp_max : dailyHigh;
      dailyLow = item.main.temp_min < dailyLow ? item.main.temp_min : dailyLow;
      sumHumidity += item.main.humidity;
      sumWindSpeed += item.wind.speed;
      if (item.rain) {
        sumRainVolume += item.rain['3h'];
      }
      if (item.snow) {
        sumSnowVolume += item.snow['3h'];
      }
      const weather = item.weather[0];
      weatherDescriptions[weather.id] = weatherDescriptions[weather.id] || {
        count: 0,
        id: weather.id,
        description: weather.description
      };
      weatherDescriptions[weather.id].count++;
    }

    const dailyWeather = Object.keys(weatherDescriptions).reduce((acc, key) => {
      return weatherDescriptions[key].count > acc.count ? weatherDescriptions[key] : acc;
    }, { count: 0 });
    return {
      date: moment.unix(list[0].dt).valueOf(),
      conditionCode: `${dailyWeather.id}`,
      conditionDesc: dailyWeather.description,
      high: dailyHigh,
      low: dailyLow
    };
  }
}
