import { Weather } from 'pi-dashboard-shared';

export interface Configuration extends Weather.Configuration {
    location: string;
    apiKey: string;
    units: string;
}
