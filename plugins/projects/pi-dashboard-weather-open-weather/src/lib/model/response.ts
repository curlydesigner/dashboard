
export namespace Response {
    export interface Main {
        /**
         * Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
         */
        temp: number;
        /**
         * Minimum temperature at the moment of calculation.
         * This is deviation from 'temp' that is possible for large
         * cities and megalopolises geographically expanded (use these parameter optionally).
         * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit
         */
        temp_min: number;
        /**
         * Maximum temperature at the moment of calculation.
         * This is deviation from 'temp' that is possible for large
         * cities and megalopolises geographically expanded (use these parameter optionally).
         * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
         */
        temp_max: number;
        /**
         * Atmospheric pressure on the sea level by default, hPa
         */
        pressure: number;
        /**
         * Atmospheric pressure on the sea level, hPa
         */
        sea_level: number;
        /**
         * Atmospheric pressure on the ground level, hPa
         */
        grnd_level: number;
        /**
         * Humidity, %
         */
        humidity: number;
    }

    export interface Wind {
        /**
         * Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
         */
        speed: number;
        /**
         * Wind direction, degrees (meteorological)
         */
        deg: number;
    }

    export interface Coordinates {
        /**
         * City geo location, longitude
         */
        lon: number;
        /**
         * City geo location, latitude
         */
        lat: number;
    }

    export interface Weather {
        /**
         * Weather condition id
         */
        id: number;
        /**
         * Group of weather parameters (Rain, Snow, Extreme etc.)
         */
        main: string;
        /**
         * Weather condition within the group
         */
        description: string;
        /**
         * Weather icon id
         */
        icon: string;
    }

    export interface Clouds {
        /**
         * Cloudiness, %
         */
        all: number;
    }

    export interface Rain {
        /**
         * Rain volume for last 1 hour, mm
         */
        '1h'?: number;
        /**
         * Rain volume for last 3 hours, mm
         */
        '3h': number;
    }

    export interface Snow {
        /**
         * Snow volume for last 1 hour
         */
        '1h'?: number;
        /**
         * Snow volume for last 3 hours
         */
        '3h': number;
    }


    export interface WeatherResponse {
        /**
         * Time of data calculation, unix, UTC
         */
        dt: number;
        /**
         * City ID
         */
        id: number;
        /**
         * City name
         */
        name: string;
        coord: Coordinates;
        weather: Weather[];
        main: Main;
        wind: Wind;
        clouds: Clouds;
        rain?: Rain;
        snow?: Snow;
        sys: {
            /**
             * Country code (GB, JP etc.)
             */
            country: string;
            /**
             * Sunrise time, unix, UTC
             */
            sunrise: number;
            /**
             * Sunset time, unix, UTC
             */
            sunset: number;
        };
    }

    export interface ForecastItem {
        /**
         * Time of data forecasted, unix, UTC
         */
        dt: number;
        main: Main;
        /**
         * https://openweathermap.org/weather-conditions
         */
        weather: Weather[];
        clouds: Clouds;
        wind: Wind;
        rain?: Rain;
        snow?: Snow;
        /**
         * Data/time of calculation, UTC
         */
        dt_txt: string;
    }

    /**
     * https://openweathermap.org/forecast5
     */
    export interface ForecastResponse {
        city: {
            /**
             * City ID
             */
            id: number;
            /**
             * City name
             */
            name: string;
            coord: Coordinates,
            /**
             * Country code (GB, JP etc.)
             */
            country: string
        };
        /**
         * Number of lines returned by this API call
         */
        cnt: number;
        list: ForecastItem[];
    }
}
