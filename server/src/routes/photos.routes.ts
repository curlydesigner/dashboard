import {Logger} from "pi-dashboard-shared";
import {PhotoManger, PhotoFile} from "../core/photos-manager";
import * as mime from 'mime-types';
import {Routes} from "./routes";
import * as config from 'config';


export class PhotosRoutes extends Routes{
    
    static screenOff = false;

    static logger = new Logger('PhotosRoutes');
    
    static photosManager = new PhotoManger(+config['photos'].refreshEvery);
    
    /**
     *  GET /photos/next/:clientId
     */
    static next(req, res) {
        PhotosRoutes.logger.debug('getNext', req.params.clientId);
        return PhotosRoutes.photosManager.next(req.params.clientId, req.query.w, req.query.h)
            .then((photo:PhotoFile) => {
                PhotosRoutes.logger.debug('send phot to client', req.params.clientId, photo.name);
                const contentType = mime.lookup(photo.name);
                res.set('Content-Type', contentType);
                res.send(photo.buffer);
            })
            .catch((error) => Routes._processError(res, error));
    }
    
    /**
     *  GET /dropbox-url
     */
    static getDropBoxAuthenticationUrl(req, res) {
        const url = PhotosRoutes.photosManager.getDropBoxAuthenticationUrl();
        res.json(url);
    }
}