import { RaspberryProvider } from "../providers/raspberry/raspberry.provider";
import { Logger } from "pi-dashboard-shared";
import { Routes } from "./routes";


export class RaspberryRoutes  extends Routes {
    static logger = new Logger('RaspberryRoutes');

    static getSensorData(req, res, raspberryProvider: RaspberryProvider) {
        raspberryProvider
        .getSensorData()
        .then(data => res.json(data))
        .catch((error) => Routes._processError(res, error));
    }
}