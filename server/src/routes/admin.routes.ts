import { Routes } from "./routes";
import { LogsProvider } from '../providers/database/logs.provider';
import * as request from 'request';


export class AdminRoutes extends Routes {
    /**
     *  GET /admin/logs
     */
    static logs(req, res) {
        const last = req.params.last ? +req.params.last : 100;
        LogsProvider
            .getLast(last)
            .then(data => res.json(data))
            .catch((error) => Routes._processError(res, error));
    }

    /**
     *  GET /proxy
     */
    static proxy(req, res) {
        console.log(req.query);
        if(!req.query.url) {
            Routes._processError(res, {
                code: 400,
                error: 'Wrong parameter'
            });
            return;
        }
        request(req.query.url).pipe(res);
    }
}