import { WebsocketServer } from './../websocket-server';
import { Logger } from "pi-dashboard-shared";
import { ScreensManager } from "../core/screens-manager";
import { Routes } from "./routes";


export class ScreensRoutes extends Routes {

    logger = new Logger('ScreensRoutes');

    manager: ScreensManager;

    constructor(server: WebsocketServer) {
        super();
        this.manager = new ScreensManager(server);
    }

    /**
     *  POST /screens
     */
    post(req, res) {
        this.logger.debug('post', req.body);
        return this.manager
            .create(req.body)
            .then(client => res.json(client))
            .catch((error) => Routes._processError(res, error));
    }


    /**
     *  PUT /screens/:id
     */
    put(req, res) {
        const id = req.params['id'];
        this.logger.debug('put', id, req.body);
        return this.manager
            .update(id, req.body)
            .then(client => res.json(client))
            .catch((error) => Routes._processError(res, error));
    }

    /**
     *  GET /screens/:id
     *  GET /screens/
     */
    get(req, res) {
        const id = req.params['id'];
        this.logger.debug('get', id);
        if (id === 'undefined') {
            return this.manager
            .createEmpty()
            .then(client => res.json(client))
            .catch((error) => Routes._processError(res, error));
        } else {
            return this.manager
                .get(req.params['id'])
                .then(response => res.json(response))
                .catch((error) => Routes._processError(res, error));
        }
    }

    /**
     *  DELETE /screens/:id
     */
    delete(req, res) {
        this.logger.debug('delete', req.params['id']);
        return this.manager
            .delete(req.params['id'])
            .then(response => res.json(response))
            .catch((error) => Routes._processError(res, error));
    }
}