import { Logger } from "pi-dashboard-shared";

export class Routes {
    static logger = new Logger('Routes');
    static _processError(res, error): void {
        if (error.code) {
            res.status(error.code).json(error.error);
        } else {
            Routes.logger.error(error);
            res.status(500).json(error.message);
        }
    }
}