import { WebsocketServer } from './../websocket-server';
import { WsMessage } from '../model/ws-message';
import * as Shared from 'pi-dashboard-shared';
import { RaspberryProvider } from '../providers/raspberry/raspberry.provider';
import { Logger, WsClientType, RemoteStatus } from 'pi-dashboard-shared';
import { LogsProvider } from '../providers/database/logs.provider';
import { RemoteCommander } from './remote-commander';

export class Commander {

    logger = new Logger('Commander');

    constructor(
        private server: WebsocketServer,
        private raspberryProvider: RaspberryProvider,
        private remoteCommander: RemoteCommander,
    ) {
        server.onMessageReceived.subscribe(this.handleMessageReceived.bind(this));
        Logger.onLog.subscribe(this.handleLogMessage.bind(this));
        remoteCommander.status$.subscribe(this.handleStatusMessage.bind(this));
    }

    handleMessageReceived(message: WsMessage) {
        if (message.clientType === WsClientType.remote) {
            this.handleRemoteMessage(message);
        } else {
            this.handleDashboardMessage(message);
        }
    }

    handleRemoteMessage(message: WsMessage) {
        this.logger.debug('handleRemoteMessage', message);
        switch (message.type) {
            case Shared.WsMessage.Type.reboot:
                this.server.close();
                this.raspberryProvider.reboot();
                break;
            case Shared.WsMessage.Type.shutdown:
                this.server.close();
                this.raspberryProvider.shutdown();
                break;
            case Shared.WsMessage.Type.reload:
                this.server.screensBroadcast({
                    type: Shared.WsMessage.Type.reload
                }, message.data);
                break;
            case Shared.WsMessage.Type.remote:
                this.remoteCommander.handleMessageReceived(message.data);
                break;
            default:
                this.logger.error('unrecognized remote message type', message);
                break;
        }
    }

    handleDashboardMessage(message: WsMessage) {
        switch (message.type) {
            case Shared.WsMessage.Type.log:
                const log: Logger.Log = message.data;
                this.logger.log(log.appName, log.appVersion, log.moduleName, log.level, log.params);
                break;
            default:
                this.logger.error('unrecognized dashboard message type', message);
                break;
        }
    }

    handleLogMessage(log: Logger.Log) {
        LogsProvider.create(log);
        this.server.broadcast(WsClientType.remote, {
            type: Shared.WsMessage.Type.log,
            data: log
        });
    }

    handleStatusMessage(status: RemoteStatus) {
        this.server.broadcast(WsClientType.remote, {
            type: Shared.WsMessage.Type.remoteStatus,
            data: status
        });
    }
    
}
