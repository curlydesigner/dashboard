import { Logger } from "pi-dashboard-shared";
import { NetworkError } from "../model/NetworkError";

export abstract class Manager {
    constructor(protected logger: Logger){}
    
    protected processError(error) {
        this.logger.error(error);
        return Promise.reject(error.code ? error : new NetworkError(500, error.message));
    }
}