import { ScreensProvider } from './../providers/database/screens.provider';
import { NetworkError } from "../model/NetworkError";
import { Manager } from "./manager";
import { Logger, Screen, WsMessage } from "pi-dashboard-shared";
import { ScreenDocument } from '../model/schemas/screen.schema';
import { WebsocketServer } from '../websocket-server';

export class ScreensManager extends Manager {

    constructor(private server: WebsocketServer) {
        super(new Logger('ScreensManager'));
    }

    create(json): Promise<Screen | NetworkError> {

        if (!json) {
            return Promise.reject(new NetworkError(400, 'Wrong body format'));
        }
        if (!json.name || json.name.length === 0) {
            return Promise.reject(new NetworkError(400, 'Screen name property is missing in the body'));
        }
        if (!json.weather) {
            return Promise.reject(new NetworkError(400, 'Weather property is missing in the body'));
        }
        if (!json.backgroundUpdate || json.backgroundUpdate.length === 0) {
            return Promise.reject(new NetworkError(400, 'Background update property is missing in the body'));
        }
        if (!json.accessToken || json.accessToken.length === 0) {
            return Promise.reject(new NetworkError(400, 'Access token property is missing in the body'));
        }
        return ScreensProvider
            .create({
                _id: undefined,
                name: json.name,
                weather: json.weather,
                backgroundUpdate: +json.backgroundUpdate
            }, json.accessToken)
            .then(this.getClientResponse)
            .catch(this.processError.bind(this));
    }

    createEmpty() {
        return ScreensProvider
        .create({})
        .then(this.getClientResponse)
        .catch(this.processError.bind(this));
    }

    update(id: string, json): Promise<Screen | NetworkError> {

        if (!json) {
            return Promise.reject(new NetworkError(400, 'Wrong body format'));
        }
        if (id !== json._id || json._id.length === 0) {
            return Promise.reject(new NetworkError(400, 'Id property is missing in the body'));
        }
        if (!json.name || json.name.length === 0) {
            return Promise.reject(new NetworkError(400, 'Screen name property is missing in the body'));
        }
        if (!json.weather) {
            return Promise.reject(new NetworkError(400, 'Weather property is missing in the body'));
        }
        if (!json.backgroundUpdate || json.backgroundUpdate.length === 0) {
            return Promise.reject(new NetworkError(400, 'Background update property is missing in the body'));
        }

        const screen: Screen = {
            name: json.name,
            weather: json.weather,
            backgroundUpdate: +json.backgroundUpdate
        };

        if(!!json.accessToken) {
            screen.accessToken = json.accessToken;
        }

        return ScreensProvider.update(id, screen)
            .then(screen => screen ? Promise.resolve(screen) : Promise.reject(new NetworkError(404, 'Client not found')))
            .then(this.getClientResponse)
            .then(screen => {
                this.server.screensBroadcast({
                    type: WsMessage.Type.screen,
                    data: screen
                }, screen._id);
                return screen;
            })
            .catch(this.processError.bind(this));
    }

    get(id: string): Promise<Screen[] | Screen | NetworkError> {
        return ScreensProvider
            .get(id)
            .then((screens): Promise<Screen[] | Screen | NetworkError> => {
                this.logger.debug('get', screens);
                if (!screens || (id && screens.length === 0)) {
                    return Promise.reject(new NetworkError(404, 'Client not found'));
                } else if (id && screens && screens.length === 1) {
                    return Promise.resolve(this.getClientResponse(screens[0]));
                } else {
                    return Promise.resolve(screens.map(this.getClientResponse));
                }
            })
            .catch(this.processError.bind(this));
    }

    delete(id: string): Promise<any> {
        return ScreensProvider
            .delete(id)
            .catch(this.processError.bind(this));
    }

    private getClientResponse({ _id, name, weather, backgroundUpdate, accessToken }: ScreenDocument): Screen {
        return {
            _id,
            name,
            weather,
            backgroundUpdate,
            hasAccessToken: !!accessToken
        }
    }
}