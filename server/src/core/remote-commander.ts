import { Logger, RemoteMessage, RemoteStatus } from 'pi-dashboard-shared';
import { OmxPlayerProvider } from '../providers/omxplayer/omxplayer-provider';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class RemoteCommander {

    logger = new Logger('RemoteCommander');

    status$: Observable<RemoteStatus>;

    constructor(
        private provider: OmxPlayerProvider,
    ) {
        this.status$ = provider.status$.pipe(
            map(playerStatus => {
                return playerStatus ? Object.assign({}, playerStatus, { playing: true }) : {
                    duration: -1,
                    position: 0,
                    paused: true,
                    playing: false,
                };
            })
        );
    }

    handleMessageReceived(message: RemoteMessage) {
        switch(message.type) {
            case RemoteMessage.Type.open:
                this.provider.openVideo(message.data);
            break;
            case RemoteMessage.Type.play:
            case RemoteMessage.Type.pause:
                this.provider.sendAction(OmxPlayerProvider.Action.pause);
            break;
            case RemoteMessage.Type.stop:
                this.provider.sendAction(OmxPlayerProvider.Action.stop);
            break;
        }
    }
}
