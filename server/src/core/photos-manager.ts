import { ScreensProvider } from './../providers/database/screens.provider';
import { NetworkError } from "../model/NetworkError";
import * as config from 'config';
import {
    DropboxProvider, ListFolderResult, FileMetadataReference,
    FileMetadata
} from "../providers/dropbox/dropbox.provider";
import { Logger } from "pi-dashboard-shared";
import * as moment from 'moment';
import * as sharp from 'sharp';
import { Manager } from "./manager";
import { ScreenDocument } from '../model/schemas/screen.schema';
import { PhotosProvider } from '../providers/database/photos.provider';
import { PhotoDocument } from '../model/schemas/photo.schema';
import { Photo } from '../model/photo';


export type PhotoFile = { buffer: any, name: string };
export type ProcessPhotoParams = { file: FileMetadata, photo: PhotoDocument, width?: number, height?: number };
export type ScreenDropboxProvider = { screen: ScreenDocument, provider: DropboxProvider };
export type DownloadPhotoParams = { photo: PhotoDocument, provider: DropboxProvider };
export type ProcessListFolderResultParams = { result: ListFolderResult, screen: ScreenDocument, provider: DropboxProvider };

export class PhotoManger extends Manager {

    constructor(private refreshEvery: number) {
        super(new Logger('PhotoManger'));
    }

    next(screenId: string, width: number, height: number): Promise<PhotoFile | NetworkError> {
        this.logger.debug('next', screenId, width, height);

        let provider: DropboxProvider;

        return this.getScreen(screenId)
            .then((data: ScreenDropboxProvider) => {
                provider = data.provider;
                return data.screen;
            })
            .then(screen => this.getPhoto(screen))
            .then(PhotosProvider.updateLastShown)
            .then(photo => ({ photo, provider }))
            .then(params => this.downloadPhoto(params))
            .then(params => this.processPhoto({ ...params, width: +width, height: +height }))
            .catch(this.processError.bind(this));
    }

    getDropBoxAuthenticationUrl() {
        return DropboxProvider.getAuthenticationUrl(config['dropbox']['appKey'], config['dropbox']['returnUrl']);
    }

    private updateFilesList(provider: DropboxProvider, screen: ScreenDocument): Promise<ScreenDropboxProvider> {
        this.logger.debug('updateFilesList', screen);
        return provider
            .getFilesList('')
            .then(result => ({ result, screen, provider }))
            .then(this.processListFolderResult.bind(this))
            .then(({ screen, provider }) => {
                screen.lasUpdated = new Date().getTime();
                return screen
                    .save()
                    .then(() => ({ screen, provider }));
            });
    }

    private processListFolderResult({ result, screen, provider }: ProcessListFolderResultParams): Promise<ScreenDropboxProvider> {
        this.logger.debug('processListFolderResult', result);

        const promises = result
            .entries
            .map((entry: FileMetadataReference) => Promise.resolve(entry)
                .then(entry => PhotosProvider.getByPath(entry.path_lower))
                .then(photoDocument => {
                    const photo: Photo = {
                        name: entry.name,
                        path: entry.path_lower,
                        screenId: screen._id,
                    }
                    const metadata = entry.media_info['metadata'];
                    if (metadata) {
                        photo.width = metadata.dimensions ? metadata.dimensions.width : undefined;
                        photo.height = metadata.dimensions ? metadata.dimensions.height : undefined;
                        photo.latitude = metadata.location ? metadata.location.latitude : undefined;
                        photo.longitude = metadata.location ? metadata.location.longitude : undefined;
                        photo.takenDate = moment(metadata.time_taken).valueOf();
                    }
                    return photoDocument ? PhotosProvider.update(photoDocument, photo) : PhotosProvider.create(photo);
                })
            );

        return Promise.all(promises)
            .then(() => {
                if (result.has_more) {
                    return provider
                        .getFilesListContinue(result.cursor)
                        .then(result => ({ result, screen, provider }))
                        .then(this.processListFolderResult.bind(this));
                } else {
                    return Promise.resolve({ screen, provider });
                }
            });
    }

    private getScreen(screenId: string): Promise<ScreenDropboxProvider> {
        this.logger.debug('getScreen', screenId);
        return ScreensProvider
            .get(screenId)
            .then(screens => screens.length === 1 ? Promise.resolve(screens[0]) : Promise.reject(new NetworkError(404, 'Screen not found')))
            .then(screen => {
                const provider = new DropboxProvider(screen.accessToken);
                if (screen.lasUpdated + this.refreshEvery < Date.now()) {
                    this.logger.debug('client last update expired', screen.lasUpdated, this.refreshEvery, Date.now());
                    return this.updateFilesList(provider, screen)
                        .then(() => ({ screen, provider }));
                } else {
                    return { screen, provider };
                }
            });
    }

    private getPhoto(screen: ScreenDocument): Promise<PhotoDocument> {
        this.logger.debug('getPhoto', screen._id);
        return PhotosProvider.getLeastShownPhoto(screen._id)
            .then(photo => photo ? Promise.resolve(photo) : Promise.reject(new NetworkError(404, 'Photo not found')));
    }

    private downloadPhoto({ photo, provider }: DownloadPhotoParams): Promise<ProcessPhotoParams> {
        this.logger.debug('downloadPhoto', photo);
        return provider
            .downloadPhoto(photo.path)
            .then(file => ({ file, photo }))
            .catch((error) => {
                if (error.status === 409) {
                    const desc = JSON.parse(error.error);
                    if (desc.error_summary === 'path/not_found/') {
                        this.logger.error('Error while downloading photo, the photo not found', photo._id, photo.path);
                        return PhotosProvider
                            .delete(photo)
                            .then(() => Promise.reject(new NetworkError(404, 'Photo not found')));
                    }
                }
                this.logger.error('Error while downloading photo', photo._id, photo.path);
                return Promise.reject(error);
            });
    }

    private processPhoto({ file, photo, width, height }: ProcessPhotoParams): Promise<PhotoFile> {
        this.logger.debug('processPhoto', photo, width, height);
        let pipe = sharp(file['fileBinary']);
        return pipe
            .rotate()
            // .resize(width, height)
            // .max()
            .toBuffer()
            .then(buffer => ({ buffer, name: photo.name }));
    }
}