import * as dgram from 'dgram';
import { Socket } from 'dgram';
import { Logger, UdpConfig, Utils } from 'pi-dashboard-shared';

export class UdpServer {

    private static EVENT_NAME = {
        message: 'message',
        listening: 'listening',
    }

    private logger = new Logger('UdpServer');
    private socket: Socket;
    private prefix = Utils.ab2str(Utils.str2ab(UdpConfig.prefix))

    constructor(private apiUrl: string) {
        this.socket = dgram.createSocket({ type: 'udp4', reuseAddr: true });
        this.socket.on(UdpServer.EVENT_NAME.message, this.onMessage.bind(this));
        this.socket.on(UdpServer.EVENT_NAME.listening, this.onListening.bind(this));
        this.socket.bind(UdpConfig.port, () => {
            this.socket.addMembership(UdpConfig.membership);
        });
    }

    private onMessage(buffer, rinfo): void {
        const message = Utils.ab2str(buffer);
        this.logger.debug('onMessage', message, rinfo.address, rinfo.port);
        if(message.indexOf(this.prefix) === 0) {
            this.sendInfoMessage(rinfo.address, rinfo.port);
        } else {
            this.logger.error('received wrong message', message, rinfo.address, rinfo.port);
        }
    }

    private sendInfoMessage(address: string, port: number) {
        const message = Buffer.from(this.apiUrl);
        const client = dgram.createSocket('udp4');
        client.send(message, port, address, (err) => {
            if(err) {
                this.logger.error('Error while sending message', err);
            }
            client.close();
        });
    }

    private onListening(): void {
        this.logger.debug('UDP Server started and listening on ', this.socket.address());
    }
}
