import { UdpServer } from './udp-server';
import { DatabaseProvider } from './providers/database/database.provider';
import { Commander } from './core/commander';
import * as bodyParser from 'body-parser';
import { Application } from 'express';
import * as express from 'express';
import * as config from 'config';
import * as ip from 'ip';

import { corsMiddleware } from './middlewares/cors.middleware';
import { UserApi } from './api/user.api';
import { PublicApi } from './api/public.api';
import { RaspberryProvider } from './providers/raspberry/raspberry.provider';
import { NodeDhtSensorEmulator, INodeDhtSensor } from './providers/raspberry/emulators/node-dht-sensor.emulator';
import { NodeDhtSensor } from './providers/raspberry/node-dht-sensor';
import { GpioEmulator, IGpio } from './providers/raspberry/emulators/gpio.emulator';
import { Gpio } from './providers/raspberry/gpio';
import { Logger } from 'pi-dashboard-shared';
import { WebsocketServer } from './websocket-server';
import { AdminApi } from './api/admin.api';
import { OmxPlayerProvider } from './providers/omxplayer/omxplayer-provider';
import { RemoteCommander } from './core/remote-commander';


require('isomorphic-fetch');

const version = require('../package.json').version;

export class Server {
  private app: Application;
  private wsServer: WebsocketServer;

  private static PORT: number = 8080;
  private port: number;


  private prefix: string;
  private raspberryProvider: RaspberryProvider;
  private omxPlayerProvider: OmxPlayerProvider;
  private databaseProvider: DatabaseProvider;
  private udpServer: UdpServer;
  private commander: Commander;
  private youtubeCommander: RemoteCommander;

  logger = new Logger('Server');

  /**
   * Bootstrap the application.
   *
   * @class Server
   * @method bootstrap
   * @static
   * @return {nIInjectorService} Returns the newly created injector for this app.
   */
  public static bootstrap(): Server {
    return new Server();
  }

  /**
   * Constructor.
   *
   * @class Server
   * @constructor
   */
  constructor() {
    Logger.appName = process.env.npm_package_name as string;
    Logger.appVersion = process.env.npm_package_version as string;

    this.databaseProvider = new DatabaseProvider(config.get('mongodb').host);

    this.port = config['serverPort'] || Server.PORT;
    this.prefix = '/api/v' + version;

    //create expressjs application
    this.app = express();

    const server = this.app.listen(this.port);
    this.wsServer = new WebsocketServer(server, this.prefix + '/ws');

    const apiAddress = `http://${ip.address()}:${this.port}${this.prefix}`
    this.logger.debug('Api url', apiAddress);

    this.udpServer = new UdpServer(apiAddress);

    let sensor: INodeDhtSensor;
    let gpio: IGpio;
    const test = config['test'];
    if (test) {
      this.logger.debug('Test mode is on');
      sensor = new NodeDhtSensorEmulator();
      gpio = new GpioEmulator();
    } else {
      sensor = new NodeDhtSensor();
      gpio = new Gpio();
    }
    this.raspberryProvider = new RaspberryProvider(sensor, gpio, config['switchOffMonitorTimeout'], test);
    this.omxPlayerProvider = new OmxPlayerProvider(test)
    this.youtubeCommander = new RemoteCommander(this.omxPlayerProvider);
    this.commander = new Commander(this.wsServer, this.raspberryProvider, this.youtubeCommander);

    //configure application
    this.config();
  }

  /**
   * Configure application
   * @class Server
   * @method config
   */
  public config() {
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ type: 'application/json' }));

    this.app.use(corsMiddleware);

    // public section
    this.app.use(express.static(__dirname + '/public'));
    this.app.use(express.static(__dirname + '/client'));
    this.app.use(this.prefix, PublicApi.config());

    // secure section
    this.app.use(this.prefix, UserApi.config(this.raspberryProvider, this.wsServer));

    this.app.use(this.prefix, AdminApi.config());


    this.app.use(this.prefix, function(req, res, next){
      res.status(404);
    
      res.format({
        json: function () {
          res.json({ error: 'Not found' })
        },
        default: function () {
          res.type('txt').send('Not found')
        }
      })
    });

    this.app.use('/', function (req, res, next) {
      return res.sendFile('/client/index.html', { root: __dirname });
    });
  }
}

Server.bootstrap();