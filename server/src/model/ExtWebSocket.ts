import { WsClientType } from 'pi-dashboard-shared';
import * as WebSocket from 'ws';

export class ExtWebSocket extends WebSocket {
    isAlive = false;
    clientType: WsClientType;
    id: string;
}
