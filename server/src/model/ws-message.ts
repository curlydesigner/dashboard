import * as Shared from "pi-dashboard-shared";

export interface WsMessage extends Shared.WsMessage {
    clientType?: Shared.WsClientType;
}

