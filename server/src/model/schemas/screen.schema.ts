
import * as mongoose from "mongoose";
import {Schema, Document} from "mongoose";
import { Model, Screen } from "pi-dashboard-shared";
import { CustomSchema } from "./custom.schema";

export type ScreenDocument = Document & Screen & Model & {
    lasUpdated: number,
};

var schema = new Schema({
    lasUpdated: { type: Number, required: true, default: 0 },
    name: { type: String, required: false },
    weather: { type: Object, required: false },
    backgroundUpdate: { type: Number, required: false },
    accessToken: { type: String, required: false },

    // meta data
    updatedOn: { type: Number, default: new Date().getTime() },
    
    ...CustomSchema
},{
    versionKey: false
});


export const ScreenSchema = mongoose.model<ScreenDocument>('screen', schema);