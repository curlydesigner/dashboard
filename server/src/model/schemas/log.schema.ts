
import {Schema, Document} from "mongoose";
import * as mongoose from "mongoose";
import { CustomSchema } from "./custom.schema";
import { Logger, Model } from "pi-dashboard-shared";

export type LogDocument = Document & Logger.Log & Model;

var schema = new Schema({
    date: {type: Number, required: true},
    appName: {type: String, required: true},
    appVersion: {type: String, required: true},
    moduleName: {type: String, required: true},
    level: {type: Number, required: true},
    params: {type: Array, required: false},

    ...CustomSchema
}, {
    versionKey: false,
    capped: { 
        size: 100000, 
        max: 100000 
    }
});

export const LogSchema = mongoose.model<LogDocument>('log', schema);