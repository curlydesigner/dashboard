import * as mongoose from "mongoose";
import {Schema, Document} from "mongoose";
import { Model } from "pi-dashboard-shared";
import { CustomSchema } from "./custom.schema";
import { Photo } from "../photo";

export type PhotoDocument = Document & Photo & Model;

var schema = new Schema({
    name: { type: String, required: true },
    path: { type: String, required: true },
    lastShown: { type: Number, required: true, default: 0 },
    height: { type: Number, required: true },
    width: { type: Number, required: true },
    latitude: { type: Number, required: false },
    longitude: { type: Number, required: false },
    takenDate: { type: Number, required: true },
    screenId: { type: String, required: true },

    // meta data
    updatedOn: { type: Number, default: new Date().getTime() },
    
    ...CustomSchema
},{
    versionKey: false
});


export const PhotoSchema = mongoose.model<PhotoDocument>('photo', schema);
