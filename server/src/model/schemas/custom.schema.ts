
export const CustomSchema = {
    // meta data
    createdOn: {type: Number, default: Date.now},
    createdBy: {type: String, default: 'System'}
};
