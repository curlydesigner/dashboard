export interface Photo {
    name:string,
    path:string,
    lastShown?:number,
    height?:number,
    width?:number,
    latitude?:number,
    longitude?:number,
    takenDate?:number,
    screenId: string
}
