import * as WebSocket from 'ws';
import * as http from 'http';
import { ExtWebSocket } from './model/ExtWebSocket';
import { Logger, WsClientType, Utils, WsClientUrlParam } from 'pi-dashboard-shared';
// import * as Shared from 'pi-dashboard-shared';
import { ReplaySubject, Subject } from 'rxjs';
import { WsMessage } from './model/ws-message';

export class WebsocketServer {
    private wss: WebSocket.Server;
    private logger = new Logger('WebsocketServer');

    public onMessageReceived: Subject<WsMessage> = new ReplaySubject();

    constructor(server, path: string) {
        this.wss = new WebSocket.Server({ server, path });
        this.wss.on('connection', this.onConnection.bind(this));

        setInterval(this.pingClients.bind(this), 5000);
    }

    public screensBroadcast(message: WsMessage, id?: string) {
        this.broadcast(WsClientType.dashboard, message, id);
    }

    public broadcast(type: WsClientType, message: WsMessage, id?: string) {
        this.wss.clients.forEach((ws: ExtWebSocket) => {
            const wsIdStr = ws.id ? ws.id.toString() : ws.id;
            const idStr = id ? id.toString() : id;
            if (ws.readyState === WebSocket.OPEN && ws.clientType === type && (!id || wsIdStr === idStr)) {
                if (message.clientType) {
                    delete message.clientType;
                }
                ws.send(JSON.stringify(message));
            }
        });
    }

    public close() {
        this.wss.clients.forEach((ws: ExtWebSocket) => ws.close());
    }

    private onConnection(ws: ExtWebSocket, req: http.IncomingMessage): void {
        const params = Utils.parseQuery(req.url);
        const type = params[WsClientUrlParam.type];
        const id = params[WsClientUrlParam.id];
        if (!type ||
            (
                type != WsClientType.dashboard &&
                type != WsClientType.remote
            )) {
            ws.close();
            this.logger.error('wrong client type', req.url);
            return;
        }
        if (type === WsClientType.dashboard && !id) {
            ws.close();
            this.logger.error('wrong client id', req.url);
            return;
        }
        this.logger.debug('new client', req.url);

        ws.id = id;
        ws.clientType = type;
        ws.isAlive = true;
        ws.on('pong', () => this.onPong(ws));
        ws.on('message', (message: string) => this.onMessage(message, ws));
        ws.on('close', this.onClose.bind(this));
    }

    private onClose(code: string, message: string) {
        this.logger.debug(`client closed with code: ${code}`, message);
    }

    private pingClients(): void {
        this.wss.clients.forEach(this.pingClient.bind(this));
    }

    private pingClient(ws: ExtWebSocket): void {
        if (!ws.isAlive) {
            this.terminateClient(ws);
            return;
        }

        ws.isAlive = false;
        // ws.send(JSON.stringify({ type: Shared.WsMessage.Type.ping }));
        ws.ping('', false, (error) => {
            if (error) {
                this.logger.error('ping client fail', error);
                this.terminateClient(ws);
            }
        });
    }

    private terminateClient(ws: ExtWebSocket): void {
        this.logger.error('terminate client', ws.url);
        ws.terminate();
    }

    private onPong(ws: ExtWebSocket) {
        ws.isAlive = true;
    }

    private onMessage(message: string, ws: ExtWebSocket) {
        try {
            const wsMessage = JSON.parse(message) as WsMessage;
            // if (wsMessage.type === Shared.WsMessage.Type.pong) {
            // this.onPong(ws);
            // } else {
            wsMessage.clientType = ws.clientType;
            this.onMessageReceived.next(wsMessage);
            // }
        } catch (error) {
            this.logger.error('error while processing message from client', message);
        }
    }
}
