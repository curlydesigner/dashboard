import { exec } from 'child_process';
import { Logger } from "pi-dashboard-shared";

export class TerminalProvider {

    constructor(protected logger: Logger, private test: boolean) {}

    runCommand(command: string, isDetached: boolean = false): Promise<string> {
        if (this.test) {
            this.logger.debug('run command: ' + command);
            return Promise.resolve('');
        } else {
            return new Promise((resolve, reject) => {
                if (isDetached) {
                    command = command + ' &'
                    exec(command, (err, stdout, stderr) => {
                        if (err) {
                            reject(err);
                        }
                    });
                    resolve('command run in detach mode ' + command);
                } else {
                    exec(command, (err, stdout, stderr) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(stdout);
                        }
                    });
                }
            });
        }
    }
}