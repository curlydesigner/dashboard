import * as exif from 'exif-reader';
import * as path from 'path'
import { Dropbox, files  } from 'dropbox';

export type ListFolderResult = files.ListFolderResult;
export type FileMetadataReference = files.FileMetadataReference;
export type FileMetadata = files.FileMetadata;
export type ThumbnailArg = files.ThumbnailArg;

export class DropboxProvider {

    accessToken: string;
    dropBox: Dropbox;
    
    constructor(accessToken: string){
        this.accessToken = accessToken;
        this.dropBox = new Dropbox({accessToken: this.accessToken});
    }

    getFilesList(path: string): Promise<ListFolderResult> {
        return this.dropBox
            .filesListFolder({path, recursive: true, include_media_info: true, limit: 20});
    }

    getFilesListContinue(cursor: string): Promise<ListFolderResult> {
        return this.dropBox
            .filesListFolderContinue({cursor});
    }

    downloadPhoto(path: string): Promise<FileMetadata> {
        // return this.dropBox
        //     .filesDownload({ path });
        return this.dropBox
            .filesGetThumbnail((<any>{ path, size: 'w2048h1536' }) as ThumbnailArg)
    }
    
    static getAuthenticationUrl(appKey: string, returnUrl: string): string{
        return new Dropbox({ clientId: appKey }).getAuthenticationUrl(returnUrl); 
    }

    // getThumbnail(path: string) {
    //     //'w32h32' | 'w64h64' | 'w128h128' | 'w256h256' | 'w480h320' | 'w640h480' | 'w960h640' | 'w1024h768' | 'w2048h1536'
    //     //'strict' | 'bestfit' | 'fitone_bestfit'
    //     return this.dropBox
    //         .filesGetThumbnail({ path, size: 'w640h480', mode: 'bestfit'});
    // }
}