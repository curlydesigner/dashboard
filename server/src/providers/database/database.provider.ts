import { Logger } from 'pi-dashboard-shared';
import * as mongoose from 'mongoose';

declare const global;

export class DatabaseProvider {

    logger = new Logger('DatabaseProvider');

    connection: mongoose.Connection;

    constructor(public host: string) {
        const options = {
            server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
            replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
        };

        this.logger.debug('connect to database:', host);
        // replace mongogoose promisses
        let tmpMongoose: any = mongoose;
        tmpMongoose['Promise'] = global.Promise;

        mongoose.connect(host, options);
        this.connection = mongoose.connection;
        this.connection.on('error', (error) => this.logger.error('db connection error', error));
    }
}
