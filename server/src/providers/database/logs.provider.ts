import { Logger } from "pi-dashboard-shared";
import { LogSchema, LogDocument } from "../../model/schemas/log.schema";

export class LogsProvider {

    static create(log: Logger.Log): any {
        const logDoc = new LogSchema({
            date: log.date,
            appName: log.appName,
            appVersion: log.appVersion,
            level: log.level,
            moduleName: log.moduleName,
            params: log.params
        });
        return logDoc.save().catch(error => console.error(error));
    }

    static getLast(last: number): Promise<Logger.Log[]> {
        return LogSchema.find({})
        .sort({ date: -1 })
        .limit(last)
        .then((logDocuments: LogDocument[]) => logDocuments.map(doc => ({
            date: doc.date,
            level: doc.level,
            appName: doc.appName,
            appVersion: doc.appVersion,
            moduleName: doc.moduleName,
            params: doc.params
        })));
    }
}
