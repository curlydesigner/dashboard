import { PhotoSchema, PhotoDocument } from "../../model/schemas/photo.schema";
import { Photo } from "../../model/photo";

export class PhotosProvider {
    static getByPath(path: string): Promise<PhotoDocument> {
        return PhotoSchema.find({path})
            .then(photos => photos && photos.length > 0 ? photos[0] : null)
    }

    static getLeastShownPhoto(screenId: string): Promise<PhotoDocument> {
        return PhotoSchema
            .find({ screenId })
            .sort({ lastShown: 1 })
            .limit(1)
            .then(photos => photos.length === 0 ? null : Promise.resolve(photos[0]));
    }

    static updateLastShown(photo: PhotoDocument, lastShown: number = Date.now()): Promise<PhotoDocument> {
        return Object.assign(photo, {lastShown})
            .save()
            .then(photo => photo);
    }

    static delete(photo: PhotoDocument): Promise<any> {
        return photo.remove();
    }

    static create(photo: Photo): Promise<PhotoDocument>{
        return new PhotoSchema(photo).save();
    }

    static update(photoDocument: PhotoDocument, photo: Photo): Promise<PhotoDocument>{
        return Object.assign(photoDocument, photo).save();
    }
}
