import { Screen, Logger } from 'pi-dashboard-shared';
import { ScreenSchema, ScreenDocument } from '../../model/schemas/screen.schema';

export class ScreensProvider {
    static logger = new Logger('ScreensProvider');
    static create(screen: Screen, accessToken?: string): Promise<ScreenDocument> {
        const client = new ScreenSchema(Object.assign(screen, {accessToken}));
        return client.save();
    }

    static update(id: string, screen: Screen): Promise<ScreenDocument> {
        ScreensProvider.logger.debug('update', id, screen);
        return ScreenSchema
            .findByIdAndUpdate(id, {$set: screen}, {new: true})
            .then(screenDocument => {
                ScreensProvider.logger.debug('updated', screenDocument);
        
                return screenDocument;
            })
    }

    static get(id?: string): Promise<ScreenDocument[]> {
        const query = id ? { _id: id } : {};
        return ScreenSchema.find(query).then(screens => screens);
    }

    static delete(id: string): Promise<any> {
        return ScreenSchema.deleteOne({ _id: id }).then(res => res);
    }
}
