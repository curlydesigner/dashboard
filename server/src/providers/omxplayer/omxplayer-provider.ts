import { TerminalProvider } from "../terminal-provider";
import { Logger } from "cd-shared";
import { from, interval, NEVER, Subscription, ReplaySubject, timer, EMPTY } from "rxjs";
import { repeatWhen, catchError, retryWhen, mergeMap } from "rxjs/operators";



export class OmxPlayerProvider extends TerminalProvider {

    static BUFFER_SIZE = 2048;
    static STATUS_INTERVAL = 1000;
    static MAX_RETRY = 15;

    status$: ReplaySubject<OmxPlayerProvider.Status> = new ReplaySubject();
    statusSubscription: Subscription;

    constructor(test: boolean) {
        super(new Logger('OmxPlayerProvider'), test);
    }

    openVideo(url: string): any {
        const command = `omxplayer \`youtube-dl -f best --buffer-size ${OmxPlayerProvider.BUFFER_SIZE} -g ${url}\` -o both`;
        const open = () => {
            return this.runCommand(command, true).then(res => {
                this.logger.debug('player started with result', res);
                this.startStatusPing();
                return res;
            });
        };
        return this.sendAction(OmxPlayerProvider.Action.stop)
            .then(open.bind(this))
            .catch(open.bind(this));
    }

    startStatusPing() {
        let firstStatusCounter = 0;
        if (this.statusSubscription) {
            this.statusSubscription.unsubscribe();
        }
        this.statusSubscription = from(this.getStatus()).pipe(
            repeatWhen(() => interval(OmxPlayerProvider.STATUS_INTERVAL)),
            retryWhen((attempts) => attempts.pipe(
                mergeMap((error) => {
                    firstStatusCounter++;
                    this.logger.error('Error to get first status, retry', error);
                    return firstStatusCounter != -1 && firstStatusCounter < OmxPlayerProvider.MAX_RETRY ? timer(1000) : EMPTY;
                })
            )),
            catchError(error => {
                this.logger.error('error get status', error);
                return NEVER;
            })
        ).subscribe(status => {
            this.logger.debug('new remote status', status);
            firstStatusCounter = -1;
            if (!status) {
                this.statusSubscription.unsubscribe();
            }
            this.status$.next(status);
        });
    }

    sendAction(action: OmxPlayerProvider.Action, data?: string): Promise<string> {
        let command = `./scripts/dbuscontrol.sh ${action}`;
        if (!!data) {
            command += ' ${data}';
        }
        return this.runCommand(command);
    }

    getStatus(): Promise<OmxPlayerProvider.Status> {
        return this.sendAction(OmxPlayerProvider.Action.status)
            .then(output => {
                this.logger.debug('player status output', output);
                if (output && output.indexOf('Error') === -1) {
                    const lines = output.split('\n');
                    const duration = +lines[0].split(':')[1].trim();
                    const position = +lines[1].split(':')[1].trim();
                    const paused = lines[0].split(':')[1].trim() === 'true';
                    return {
                        duration,
                        position,
                        paused
                    };
                } else {
                    return null;
                }
            });
    }

}

export namespace OmxPlayerProvider {
    export enum Action {
        stop = 'stop',
        pause = 'pause',
        status = 'status',
        seek = 'seek',
        volumeup = 'volumeup',
        volumedown = 'volumedown',
        setposition = 'setposition', // [position in microseconds]
    }

    export interface Status {
        duration: number;
        position: number;
        paused: boolean;
    }
}