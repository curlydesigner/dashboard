import { INodeDhtSensor } from './emulators/node-dht-sensor.emulator';
import { IGpio } from './emulators/gpio.emulator';
import { Logger, DHTSensorData } from 'pi-dashboard-shared';
import { TerminalProvider } from '../terminal-provider';

export class RaspberryProvider extends TerminalProvider {

    /**
     * DHT11	            11
     * DHT22 or AM2302	    22
     */
    static DHT11_TYPE = 22;
    static SENSOR_PIN = 18;

    logger = new Logger('RaspberryProvider');

    pir: IGpio;

    sensor: INodeDhtSensor;

    isMonitorSwitchedOn: boolean = true;
    timeoutHandle: any;

    constructor(sensor: INodeDhtSensor, gpio: IGpio, private switchOffTimeout: number, test: boolean) {
        super(new Logger('RaspberryProvider'), test);

        this.sensor = sensor;
        this.pir = gpio;
        gpio
            .init()
            .then(() => {
                this.pir.watch(this.watch.bind(this));
                this.resetSwitchOffTimer();
            });
    }

    getSensorData(): Promise<DHTSensorData> {
        return new Promise((resolve, reject) => {
            this.sensor.read(
                RaspberryProvider.DHT11_TYPE,
                RaspberryProvider.SENSOR_PIN,
                (err: any, temperature: number, humidity: number) => {
                    if (err) {
                        this.logger.error('error while getting sensor data', err);
                        reject(err);
                    } else {
                        this.logger.debug('received', temperature, humidity);
                        resolve({ temperature, humidity });
                    }
                }
            );
        });
    }

    watch(err, value) {
        this.logger.debug('watch', err, value);
        if (err) {
            this.logger.error('error while watch for pir', err);
        } else {
            this.logger.debug('check monitor action', this.isMonitorSwitchedOn, value);
            if (!this.isMonitorSwitchedOn && value === 1) {
                this.switchMonitorOnOff(true);
            } else if (this.isMonitorSwitchedOn && value === 1) {
                this.resetSwitchOffTimer();
            }
        }
    }

    resetSwitchOffTimer() {
        this.logger.debug('resetSwitchOffTimer', this.switchOffTimeout);
        // reset timer
        if (this.timeoutHandle) {
            clearTimeout(this.timeoutHandle);
        }
        this.timeoutHandle = setTimeout(() => {
            this.switchMonitorOnOff(false);
        }, this.switchOffTimeout);
    }

    switchMonitorOnOff(on: boolean) {
        this.logger.debug('switchMonitorOnOff', on);
        this.isMonitorSwitchedOn = on;
        let command = `vcgencmd display_power ${(on ? '1' : '0')}`;
        return this.runCommand(command);
    }

    exit() {
        this.pir.unExport();
    }

    reboot() {
        const command = `sudo reboot`;
        return this.runCommand(command);
    }

    shutdown() {
        const command = `sudo shutdown -P now`;
        return this.runCommand(command);
    }
}