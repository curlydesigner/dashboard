import { INodeDhtSensor } from "./emulators/node-dht-sensor.emulator";

export class NodeDhtSensor implements INodeDhtSensor {

    sensor: NodeDhtSensor;

    constructor() {
        const nodeDhtSensorModule = 'node-dht-sensor';
        import(nodeDhtSensorModule)
        .then(sensor => {
            this.sensor = sensor;
        });
    }

    read(sensorType: number, gpioPin: number, callback: (err: any, temperature: number, humidity: number) => void): void {
        this.sensor.read(sensorType, gpioPin, callback);
    }
}
