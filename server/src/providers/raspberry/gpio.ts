import { IGpio } from './emulators/gpio.emulator';
import { Gpio as OnoffGpio } from 'onoff'; 

export class Gpio implements IGpio {
    static PIR_OUTPUT_PIN = 17;
 
    pir: OnoffGpio;

    constructor() {}

    init(): Promise<void> {
        const onoffModule = 'onoff';
        return import(onoffModule)
        .then(onoff => {
            this.pir = new onoff.Gpio(Gpio.PIR_OUTPUT_PIN, 'in', 'both');
        });
    }

    watch(cb: (error: Error, value: number) => void): void {
        this.pir.watch((error, value) => cb(error, value));
    }

    unExport(): void {
         this.pir.unexport();
    }

}
