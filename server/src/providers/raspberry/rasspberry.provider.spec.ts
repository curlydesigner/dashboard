import { INodeDhtSensor } from './emulators/node-dht-sensor.emulator';
import { IGpio } from './emulators/gpio.emulator';
import * as mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { RaspberryProvider } from './raspberry.provider';

const values = [{ value: 1, timeout: 0 }, { value: 1, timeout: 900 }, { value: 0, timeout: 100 }];
class GpioMock implements IGpio{
    init(): Promise<void> {
        throw new Error("Method not implemented.");
    }
    unexport(): void {
        throw new Error("Method not implemented.");
    }
    watch(callback: (err, value) => void) {
        for (let item of values) {
            setTimeout(() => {
                callback(null, item.value);
            }, item.timeout);
        }
    }
}

class NodeDhtSensorMock implements INodeDhtSensor {
    read(sensorType: number, gpioPin: number, callback: (err: any, temperature: number, humidity: number) => void): void {
        throw new Error("Method not implemented.");
    }

}

describe('unit-tests', () => {
    describe('RasspberrryProvider', () => {
        it('should switch on screen', (done) => {

            const provider = new RaspberryProvider(new NodeDhtSensorMock(), new GpioMock(), 1000, true);
            sinon.spy(provider, 'switchMonitorOnOff');

            setTimeout(() => {
                expect(provider.switchMonitorOnOff['callCount']).to.eqls(2);
                //resolve();
                done();
            }, 3000);
        }).timeout(5000);
    });
});