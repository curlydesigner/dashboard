
export interface INodeDhtSensor {
    read(sensorType: number, gpioPin: number, callback: (err: any, temperature: number, humidity: number) => void): void;
}

export class NodeDhtSensorEmulator implements INodeDhtSensor {
    read(sensorType: number, gpioPin: number, callback: (err: any, temperature: number, humidity: number) => void): void {
        callback(null, Math.floor(Math.random() * 40) + 1, Math.floor(Math.random() * 90) + 1);
    }
}
