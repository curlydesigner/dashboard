import { Logger } from 'pi-dashboard-shared';


export interface IGpio {
    init(): Promise<void>;
    watch(cb:(error:Error, value:number) => void):void;
    unExport():void;
}

export class GpioEmulator implements IGpio {

    logger = new Logger('GpioEmulator');

    init(): Promise<void> {
        return Promise.resolve();
    }

    watch(cb:(error:Error, value:number) => void):void {
        setInterval(() => {
            cb(null, Math.round(Math.random()));
        }, 5000);
    }

    unExport(): void {
        this.logger.debug('unexport');
    }
}
