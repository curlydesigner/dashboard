import { WebsocketServer } from './../websocket-server';
import { Application } from 'express';
import * as express from 'express';
import { PhotosRoutes } from '../routes/photos.routes';
import { ScreensRoutes } from '../routes/screens.routes';
import { RaspberryProvider } from '../providers/raspberry/raspberry.provider';
import { RaspberryRoutes } from '../routes/raspberry.routes';

export class UserApi {
    static config(raspberryProvider: RaspberryProvider, server: WebsocketServer): Application {
        const app: Application = express();
        const screenRoutes = new ScreensRoutes(server);

        app.route('/photos/next/:clientId')
            .get(PhotosRoutes.next);

        app.route('/dropbox-authentication-url')
            .get(PhotosRoutes.getDropBoxAuthenticationUrl);

        app.route('/screens')
            .post(screenRoutes.post.bind(screenRoutes))
            .get(screenRoutes.get.bind(screenRoutes));

        app.route('/screens/:id')
            .get(screenRoutes.get.bind(screenRoutes))
            .put(screenRoutes.put.bind(screenRoutes))
            .delete(screenRoutes.delete.bind(screenRoutes));

        app.route('/raspberry/get-sensor-data')
            .get((req, res) => RaspberryRoutes.getSensorData(req, res, raspberryProvider));

            return app;
    }
}