import { Application } from 'express';
import * as express from 'express';
import { AdminRoutes } from '../routes/admin.routes';

export class AdminApi {
    static config(): Application {
        const app: Application = express();

        app.route("/admin/logs")
            .get((req, res) => AdminRoutes.logs(req, res));


        app.route('/proxy')
            .get((req, res) => AdminRoutes.proxy(req, res));

        return app;
    }
}