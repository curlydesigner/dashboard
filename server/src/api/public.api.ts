import { Application } from 'express';
import * as express from 'express';

export class PublicApi {
    static config(): Application {
        const app:Application = express();
        return app;
    }
}