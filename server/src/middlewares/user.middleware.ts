//
// import { Request, Response, NextFunction } from 'express';
// import {default as User, UserModel} from "../model/user.model";
// import {Logger} from "../logger";
// import * as config from 'config';
// import {Lookups} from "../logic/lookups";
//
// const logger = new Logger('userMiddleware');
//
// export const userMiddleware = (req: Request, res: Response, next: NextFunction) => {
//     if(req.method === Lookups.responseMethods.options){
//         next();
//         return;
//     }
//     User
//         .findById(req[Lookups.requestCustomKeys.token].id)
//         .select('name email admin accounts activated')
//         .exec()
//         .then((user: UserModel) => {
//             if(user && user.activated){
//                 req[Lookups.requestCustomKeys.user] = JSON.parse(JSON.stringify(user));
//                 next();
//             } else if(user) {
//                 logger.error('user is not activated', req[Lookups.requestCustomKeys.token].id);
//                 return res.status(403).send('Not Activated.');
//             } else {
//                 logger.error('user access Denied', req[Lookups.requestCustomKeys.token].id);
//                 return res.status(401).send('Access Denied.');
//             }
//         })
//         .catch(err => res.status(err.status ? err.status : 500).send(config.get(Lookups.configKeys.debug) ? err : 'Server error.'));
//
// };