//
// import { Request, Response, NextFunction } from 'express';
// import * as config from 'config';
// import * as jwt from 'jsonwebtoken';
//
// import {Logger} from "../logger";
// //import {Lookups} from "../logic/lookups";
//
// const logger: Logger = new Logger('tokenMiddleware');
//
// export const tokenMiddleware = (req: Request, res: Response, next: NextFunction) => {
//
//   console.log(req.method);
//   if(req.method === Lookups.responseMethods.options){
//     next();
//     return;
//   }
//   // check header or url parameters or post parameters for token
//   var token = req.body.token || req.query.token || req.headers['x-access-token'];
//
//   // decode token
//   if (token) {
//
//     // verifies secret and checks exp
//     jwt.verify(token, config.get(Lookups.configKeys.secret), function verify(err, decoded) {
//       if (err) {
//         logger.error('token authentication failed', token);
//         return res.status(401).json({ status: 401, error: 'AuthenticationTokenFailed'});
//       } else {
//         // if everything is good, save to request for use in other routes
//         //req[Lookups.requestCustomKeys.token] = decoded;
//         next();
//       }
//     });
//
//   } else {
//     logger.error('no token provided', req.url);
//     return res.status(403).json({ status: 403, error: 'WrongAuthenticationToken' });
//
//   }
// };