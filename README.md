# Raspberry Pi Dashboard
This applications convert screen connected to Raspberry PI to weather/photo station.
This application includes three parts:
- Client - SPA Angular application that displays weather and run photos as background.
- Server - NodeJS Express application that make connection between client and drop box. In addition this application manage sensors connected to Raspbery PI.
- Remote - Ionic Mobile Hybrid application that controls client and server.

## Pre-requirements
### HC-SR501 Motion Sensor Detector [Optional]
When this sensor connected to the board, the server will automatically switch the screen off when no one in front of the sensor for some periud of time. 
After connecting of the sensor please install onoff library in the server folder.
```
$ npm install onoff
```
### DHT11/22 Themperature and Relative Humidity Sensor [Optional]
When this sensor installed, client will show indoor themerature and humidity.
This sensor requires instalation of BCM2835 library and node-dht-sensor module on the server.
Please follow the installation instruction on the page: http://www.airspayce.com/mikem/bcm2835/
```
$ npm install node-dht-sensor
```
## Installation
### NodeJS
Add to profile the pathes
```
$ sudo nano /etc/profile
```
```
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin"
```
Download NodeJS latest version, unzip and move to system folders
```
$ wget https://nodejs.org/dist/latest-v5.x/node-v5.11.0-linux-armv7l.tar.gz
$ tar -xvzf node-v5.11.0-linux-armv7l.tar.gz
$ sudo mv node-v5.11.0-linux-armv7l /opt/node
$ sudo mkdir /opt/bin
$ sudo ln -s /opt/node/bin/* /opt/bin/
```

### Mongodb
```
$ sudo apt-get update
$ sudo apt-get upgrade
$ sudo apt-get install mongodb-server
$ sudo mkdir -p /data/db
$ sudo service mongodb start
```
### Set Raspberry Pi in Chrome Kiosk mode
To make the browser full screen and without menu, the browser should start in kiosk mode. In addition the screen should stay on all the time.
```
$ sudo nano ~/.config/lxsession/LXDE-pi/autostart
``` 
Replace the content of the file with:
```
@xset s off
@xset -dpms
@xset s noblank
@chromium-browser --no-startup-window --kiosk
@chromium-browser --noerrordialogs --kiosk http://localhost:8081
```
- `@chromium-browser --no-startup-window --kiosk` - the configuration requires two browser start up commands, this prevent showing restore session warnings when the rasspberry was restarted.
- `http://localhost:8081` - replace this url with your url if the screen connected to other server.

Hide the mouse pointer after few moments of inactivity.
```
$ sudo apt-get install unclutter
```
Change display configuration
```
$ sudo nano /boot/config.txt
```
```
# Display orientation. Landscape = 0, Portrait = 1
display_rotate=1

# Use 24 bit colors
framebuffer_depth=24
```
### Set server

Running the server on boot:
```
sudo nano /etc/rc.local
```
On the line before `exit 0` write the following script, replacing `NAME_OF_DIRECTORY` with the directory of your application
```
su pi -c 'node /home/pi/NAME_OF_DIRECTORY/server.js < /dev/null &'
```
Write out the lines in order to save them (CTRL-X) and then `$ sudo reboot` to restart your RPi

## TODO
- add https free certificate
- websocket: make my ping/pong (rolled back to original)
### Server
- add plugins support, each plugin will have the script with functionality and configuration UI for Remote and Client
### Client
### Remote
- Discover should show list of servers
- Server section should show configuration for moving sensor