export const UdpConfig = {
    port: 8082,
    membership: '239.255.255.250',
    prefix: 'pi-dashboard'
};
