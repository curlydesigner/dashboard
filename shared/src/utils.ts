export class Utils {
    static parseQuery(uri: string) {
        if (uri && uri.length > 0) {
            const splittedUri = uri.split('?');
            const query = splittedUri.length === 2 ? splittedUri[1] : splittedUri[0];
            return query
                .split('&')
                .reduce((acc, part) => {
                    const keyVal = part.split('=');
                    acc[keyVal[0]] = keyVal[1];
                    return acc;
                }, {});
        }
        return {};
    }

    /**
     * convert string to ArrayBuffer - taken from Chrome Developer page
     * @param str 
     */
    static str2ab(str) {
        var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
        var bufView = new Uint16Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    static ab2str(buf) {
        return String.fromCharCode.apply(null, new Uint8Array(buf));
    }
}
