import * as Weather from './weather';

export interface Screen {
    _id?: string,
    name?: string,
    backgroundUpdate?: number,
    accessToken?: string,
    hasAccessToken?: boolean,
    weather?: Weather.Configuration;
}