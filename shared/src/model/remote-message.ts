
export interface RemoteMessage {
    type: RemoteMessage.Type;
    data?: any;
}

export namespace RemoteMessage {
    export enum Type {
        play = 'play',
        pause = 'pause',
        stop = 'stop',
        next = 'next',
        previous = 'previous',
        open = 'open',
        setVolume = 'volume',
        switchMute = 'switchMute',
        setShuffle = 'setShuffle'
    }
}

export interface RemoteStatus{
    duration: number;
    position: number;
    paused: boolean;
    playing: boolean;
}