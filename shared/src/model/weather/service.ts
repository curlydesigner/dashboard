import { Data } from ".";
import { Observable } from "rxjs";

export interface Service {
    getWeatherData(): Observable<Data>;
}