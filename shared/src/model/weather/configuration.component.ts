import { Configuration } from ".";
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

export interface ConfigurationComponentConstructor {
    new(formBuilder: FormBuilder, http: HttpClient): ConfigurationComponent;
}

export interface ConfigurationComponentMetadata {
    component: ConfigurationComponentConstructor;
    name: string;
    type: string;
}

export interface ConfigurationComponent {
    config: Configuration;
    readonly valid: boolean;
    readonly value: Configuration;

    ngOnChanges();
}