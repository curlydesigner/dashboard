import { Forecast } from "./forecast";

export interface Data {
    type: string;
    date: number;
    title: string;
    temp: number;
    feelsLike: number;
    /**
    * Sunrise time milliseconds
    */
    sunrise: number;
    /**
    * Sunset time milliseconds
    */
    sunset: number;
    /**
    * Humidity, %
    */
    humidity: number;
    conditionCode: string;
    conditionDesc: string;
    wind: {
        speed: number;
        direction: number;
    };
    forecast: Forecast[];
}