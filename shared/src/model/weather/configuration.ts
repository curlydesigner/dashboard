export interface Configuration {
    type: string;
    proxyUrl: string;
}