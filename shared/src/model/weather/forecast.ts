export interface Forecast {
    conditionCode: string;
    conditionDesc: string;
    date: number;
    high: number;
    low: number;
}
