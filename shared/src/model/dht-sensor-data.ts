export interface DHTSensorData {
    temperature: number;
    humidity: number;
}
