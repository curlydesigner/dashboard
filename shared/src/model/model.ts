
export type Model = {
    _id: string;
    
    updatedOn?: number;
    createdOn?: number;
    createdBy?: string;
};
