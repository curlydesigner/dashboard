
export interface WsMessage {
    type: WsMessage.Type;
    data?: any;
}

export namespace WsMessage {
    export enum Type {
        shutdown = 'shutdown',
        reboot = 'reboot',
        reload = 'reload',
        log = 'log',
        screen = 'screen',
        ping = 'ping',
        pong = 'pong',
        remote = 'remote',
        remoteStatus = 'remoteStatus'
    }
}

