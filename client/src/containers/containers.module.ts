import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import {ComponentsModule} from '../components/components.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardModule} from './dashboard/dashboard.module';
import {MaterialModule} from '../app/material.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    MaterialModule,
    ComponentsModule,
    DashboardModule
  ],
  declarations: [
  ],
  exports: [
    DashboardComponent,
  ]
})
export class ContainersModule { }
