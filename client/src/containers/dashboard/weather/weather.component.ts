import {Component, OnChanges} from '@angular/core';
import {Input} from '@angular/core';
import * as moment from 'moment';
import { Logger, DHTSensorData, Weather } from 'pi-dashboard-shared';
import { windSpeedToBeaufort } from 'src/providers/weather/weather-service';

@Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.css'],
})
export class WeatherComponent implements OnChanges {
    @Input() weatherData: Weather.Data;
    @Input() dhtSensorData: DHTSensorData;

    logger = new Logger('WeatherComponent');

    constructor() {}

    ngOnChanges() {
        this.logger.debug('Weather updated');
    }

    getWindDirectionClass(direction: string): string {
        return 'from-' + direction + '-deg';
    }

    getWindBeaufortClass(windSpeed: number): string {
        const beaufort = windSpeedToBeaufort(windSpeed);
        return beaufort ? 'wi-wind-beaufort-' + beaufort.grade.toFixed(0) : '';
    }

    getConditionClass(type: string, conditionCode: number): string {
        const typeStr = type ? `-${type}` : '';
        return conditionCode ? `wi${typeStr}-${conditionCode}` : '';
    }

    formatForecastDate(date: number): string {
        return moment(date).format('DD MMM');
    }

    formatTime(time: number) {
        return moment(time).format('hh:mm a'); // , 'H:m a').format('HH:mm a');
    }

    formatDate(date: number): string {
        return moment(date).format('DD MMM hh:mm a');
    }

    formatWeekDay(date: number): string {
        return moment(date).format('ddd');
    }
}
