import { Routes } from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {ConfigurationResolve} from '../../guards/configuration-resolve';

export const dashboardRoutes: Routes = [{
  path: '',
  component: DashboardComponent,
  resolve: { hasConfiguration: ConfigurationResolve },
}];

