import { Component, ElementRef } from '@angular/core';
import { Observable, EMPTY, Subscription, throwError, NEVER } from 'rxjs';
import { map, flatMap, combineLatest, tap, catchError } from 'rxjs/operators';
import { Screen, DHTSensorData, Logger, Weather } from 'pi-dashboard-shared';
import { MatDialog } from '@angular/material';
import { ConfigurationDialogComponent, DialogData } from './configuration-dialog/configuration-dialog.component';
import { CoreProvider, Services } from 'src/providers/core.provider';
import { ErrorProvider } from 'src/providers/error.provider';

@Component({
  selector: 'app-dashboard-component',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  weatherData$: Observable<Weather.Data>;
  dhtSensorData$: Observable<DHTSensorData>;
  bgImageUrl$: Observable<string>;
  screen$: Observable<Screen>;
  servicesSubscription: Subscription;
  services: Services;

  hasConfiguration: boolean;

  showConfigButton = true;

  logger = new Logger('DashboardComponent');

  constructor(
    private errorProvider: ErrorProvider,
    private el: ElementRef,
    public dialog: MatDialog,
    private coreProvider: CoreProvider) {
    this.screen$ = coreProvider.getScreen();

    this.weatherData$ = this.coreProvider
      .getServices()
      .pipe(
        combineLatest(this.screen$),
        flatMap(([services, screen]) => {
          if (!services.weatherService) {
            this.logger.debug('The weather service was not initialized');
            return NEVER;
          } else {
            return services.weatherService.getWeatherData();
          }
        }),
        catchError(error => {
          if (error.status === 0) {
            return throwError(error);
          } else {
            this.logger.error('getSensorData', error);
            return NEVER;
          }
        }),
        catchError(this.catchError.bind(this))
      );

    this.dhtSensorData$ = this.screen$.pipe(flatMap(() => coreProvider.getSensorData()));

    this.bgImageUrl$ = this.screen$.pipe(
      flatMap((screen: Screen) => {
        const width = this.el.nativeElement.clientWidth;
        const height = this.el.nativeElement.clientHeight;
        if (!screen._id || !screen.backgroundUpdate) {
            return NEVER;
        } else {
          return coreProvider
            .getNextPhoto(screen._id, screen.backgroundUpdate, width, height)
            .pipe(
              map(this.mapToURL.bind(this)),
              map((url: string) => url)
            );
        }
      }),
      catchError(this.catchError.bind(this))
    );

    this.servicesSubscription = this.coreProvider.getServices()
      .subscribe(services => {
        this.services = services;
        this.logger.debug('Check configuration', this.services.configService.screen);
        if (!this.services.configService.hasConfiguration()) {
          this.openConfigurationDialog();
        }
      });
  }

  openConfigurationDialog() {
    if (!!this.services) {
      this.showConfigButton = false;
      const dialogRef = this.dialog.open(ConfigurationDialogComponent, {
        width: '600px',
        data: {
          hasServer: this.services.configService.hasServer(),
          screen: this.services.configService.screen,
          dataService: this.services.dataService,
          weatherService: this.services.weatherService,
          configService: this.services.configService
        } as DialogData
      });
      dialogRef.afterClosed().subscribe(() => {
        this.showConfigButton = true;
      });
    }
  }

  mapToURL(blob: any): string {
    const url = window.URL.createObjectURL(blob);
    return 'url(' + url + ')';
  }

  handleConfigurationClick() {
    this.openConfigurationDialog();
  }

  catchError(error: any): Observable<any> {
    if (error.status === 0) {
      return this.errorProvider.communication();
    }
    return throwError(error);
  }
}
