import { ConfigurationDialogComponent } from './configuration-dialog/configuration-dialog.component';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from './dashboard.component';
import { WeatherComponent } from './weather/weather.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../app/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ConfigButtonComponent } from './config-button/config-button.component';
import { WeatherConfigurationHostDirective } from './configuration-dialog/weather-configuration-host.directive';

@NgModule({
    imports: [
        ReactiveFormsModule,
        RouterModule,
        FormsModule,
        CommonModule,
        BrowserModule,
        MaterialModule,
        ComponentsModule
    ],
    declarations: [
        DashboardComponent,
        WeatherComponent,
        ConfigButtonComponent,
        ConfigurationDialogComponent,
        WeatherConfigurationHostDirective
    ],
    exports: [
        DashboardComponent,
    ],
    providers: [
    ],
    entryComponents: [
        ConfigurationDialogComponent
    ]
})
export class DashboardModule { }

