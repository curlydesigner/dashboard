import {Component, HostListener, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-config-button',
    templateUrl: './config-button.component.html',
    styleUrls: ['./config-button.component.css'],
})
export class ConfigButtonComponent {
    @Output() click: EventEmitter<any> = new EventEmitter();
    showButton = false;

    timer: NodeJS.Timer;

    @HostListener('document:mousemove', ['$event'])
    onMouseMove() {
        this.showButton = true;
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.showButton = false;
        }, 5000);
    }
}
