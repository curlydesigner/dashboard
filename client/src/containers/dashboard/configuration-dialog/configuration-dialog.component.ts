import { Component, OnInit, Inject, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { DataService } from '../../../providers/data/data-service';
import { ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { combineLatest, merge, share, tap } from 'rxjs/internal/operators';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorProvider } from '../../../providers/error.provider';
import { Logger, Screen, Weather } from 'pi-dashboard-shared';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Utils } from 'pi-dashboard-shared';
import { weatherConfigurationMetadata } from 'src/providers/weather/weather-service';
import { WeatherConfigurationHostDirective } from './weather-configuration-host.directive';
import { ConfigurationService } from 'src/providers/configuration-service';

export interface DialogData {
    hasServer: boolean;
    screen: Screen;
    dataService: DataService;
    weatherService: Weather.Service;
    configService: ConfigurationService;
}

@Component({
    selector: 'app-configuration-dialog',
    templateUrl: './configuration-dialog.component.html',
    styleUrls: ['./configuration-dialog.component.css']
})
export class ConfigurationDialogComponent implements OnInit {

    logger = new Logger('ConfigurationDialogComponent');

    STORAGE_CONFIG_KEY = 'tmp-config';

    intervals = [
        { text: '1 Minute', value: 60 * 1000 },
        { text: '2 Minutes', value: 2 * 60 * 1000 },
        { text: '5 Minutes', value: 5 * 60 * 1000 },
        { text: '10 Minutes', value: 10 * 60 * 1000 },
        { text: '15 Minutes', value: 15 * 60 * 1000 },
    ];

    hasServer = false;
    loading = true;
    accessToken: string;

    dropBoxUrl$: Observable<string>;
    screens$: Observable<Screen[]>;

    formGroup: FormGroup;

    showCancel = false;

    initialName: string;

    error: string;

    weatherProviders = weatherConfigurationMetadata;
    weatherConfigurationComponent: Weather.ConfigurationComponent;

    @ViewChild(WeatherConfigurationHostDirective) weatherConfigHost: WeatherConfigurationHostDirective;

    constructor(
        private errorProvider: ErrorProvider,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<ConfigurationDialogComponent>,
        private componentFactoryResolver: ComponentFactoryResolver,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {

        this.hasServer = data.hasServer;
        let screen: Screen = data.screen;
        this.showCancel = !!screen;

        const params = Utils.parseQuery(this.route.snapshot.fragment);
        // Restore form when returns from dropbox
        if (params['access_token']) {
            const tmpConfStr = localStorage.getItem(this.STORAGE_CONFIG_KEY);
            if (tmpConfStr) {
                this.accessToken = params['access_token'];
                screen = JSON.parse(tmpConfStr);
            }
        }
    }

    ngOnInit() {
        const screen = this.data.screen;
        const weatherType = screen.weather ? screen.weather.type : null;
        this.formGroup = this.formBuilder.group({
            screen: [screen && screen._id ? screen._id : '0'],
            screenName: [screen ? screen.name : null, Validators.required],
            backgroundUpdate: [screen ? screen.backgroundUpdate : null, Validators.required],
            accessToken: [this.accessToken ? this.accessToken : (screen && screen.hasAccessToken ? 'HasToken' : null), Validators.required],
            weatherProviderType: weatherType,
            weather: screen.weather ? screen.weather : null,
        });
        this.showWeatherProviderConfiguration(weatherType);

        if (this.hasServer) {
            this.dropBoxUrl$ = this.data.dataService.getDropBoxAuthenticationUrl().pipe(share());
            this.screens$ = this.data.dataService.getScreens().pipe(share());

            this.dropBoxUrl$
                .pipe(merge(this.screens$),
                    catchError(error => {
                        this.loading = false;
                        if (error.status === 0) {
                            return this.errorProvider.communication();
                        } else if (error.status) {
                            this.error = 'Something bad happened; please try again later.';
                        }
                        return throwError(this.error);
                    })
                )
                .subscribe(() => this.loading = false);

            this.formGroup.controls.screen
                .valueChanges
                .pipe(
                    combineLatest(this.screens$)
                )
                .subscribe(([id, screens]) => {
                    const filteredScreens = screens.filter(s => s._id === id);
                    this.patchForm(filteredScreens.length === 1 ? filteredScreens[0] : null, false);
                });
        } else {
            this.loading = false;
        }

        this.formGroup.controls.weatherProviderType
            .valueChanges
            .subscribe(this.showWeatherProviderConfiguration.bind(this));

    }

    showWeatherProviderConfiguration(type: string) {
        const viewContainerRef = this.weatherConfigHost.viewContainerRef;
        viewContainerRef.clear();
        if (type) {
            const configMetadata = weatherConfigurationMetadata.find(meta => meta.type === type);
            const componentFactory = this.componentFactoryResolver.resolveComponentFactory(configMetadata.component);

            const componentRef = viewContainerRef.createComponent(componentFactory);
            this.weatherConfigurationComponent = componentRef.instance as Weather.ConfigurationComponent;
            this.weatherConfigurationComponent.config = this.formGroup.controls.weather.value
                && this.formGroup.controls.weather.value.type === type
                ? this.formGroup.controls.weather.value
                : {} as Weather.Configuration;
            this.weatherConfigurationComponent['ngOnChanges']();
        }
    }

    handleSaveClient() {
        if (this.isValid()) {
            localStorage.removeItem(this.STORAGE_CONFIG_KEY);
            this.loading = true;
            const screen = this.getScreen();
            this.data.configService
                .saveScreen(screen)
                .subscribe(() => {
                    this.loading = false;
                    this.dialogRef.close();
                });
        }
    }

    handleRemoveClient() {
        const id = this.formGroup.controls.screen.value;
        this.loading = true;
        this.data.dataService
            .removeScreen(id)
            .subscribe(() => {
                this.loading = false;
                this.patchForm(null);
                this.screens$ = this.data.dataService.getScreens();
            });
    }

    handleConnectClick() {
        localStorage.setItem(this.STORAGE_CONFIG_KEY, JSON.stringify(this.getScreen()));
    }

    handleConnectDropboxChange($event) {
        if (!$event.checked) {
            this.formGroup.patchValue({ accessToken: undefined });
        }
    }

    isValid() {
        return (this.hasServer
            && this.formGroup.valid
            && this.weatherConfigurationComponent
            && this.weatherConfigurationComponent.valid)
            || (!this.hasServer && this.weatherConfigurationComponent && this.weatherConfigurationComponent.valid);
    }

    isNewScreenSelected() {
        return this.formGroup.controls.screen.value === '0';
    }

    getScreen(): Screen {
        const controls = this.formGroup.controls;
        const screenId = controls.screen.value;
        return {
            _id: screenId === '0' ? undefined : screenId,
            name: controls.screenName.value,
            weather: this.weatherConfigurationComponent ? Object.assign(
                {},
                this.weatherConfigurationComponent.value,
                { type: controls.weatherProviderType.value }
            ) : null,
            backgroundUpdate: controls.backgroundUpdate.value,
            accessToken: controls.accessToken.value === 'HasToken'
                ? undefined
                : controls.accessToken.value,
        } as Screen;
    }

    patchForm(screen?: Screen, updateScreen: boolean = true) {
        const serverPatch: any = {
            screenName: screen ? screen.name : null,
            backgroundUpdate: screen ? screen.backgroundUpdate : null,
            accessToken: screen && screen.hasAccessToken ? 'HasToken' : null,
            weatherProviderType: screen ? screen.weather.type : null,
            weather: screen ? screen.weather : null
        };
        if (updateScreen) {
            serverPatch.screen = screen ? screen._id : '0';
        }
        this.formGroup.patchValue(serverPatch);
        if (screen && screen.weather.type) {
            this.weatherConfigurationComponent.config = screen.weather;
            this.weatherConfigurationComponent['ngOnChanges']();
        }
    }
}
