import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appWeatherConfigurationHost]',
})
export class WeatherConfigurationHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
