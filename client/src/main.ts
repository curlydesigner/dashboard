import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Logger } from 'pi-dashboard-shared';

declare const require;
const appName = require('../package.json').name;
const appVersion = require('../package.json').version;

Logger.appName = appName;
Logger.appVersion = appVersion;
new Logger('main').debug('Start application');


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
