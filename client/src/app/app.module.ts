import {NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app.routing';
import {ContainersModule} from '../containers/containers.module';
import {RouterModule} from '@angular/router';
import {ConfigurationResolve} from '../guards/configuration-resolve';
import {ErrorProvider} from '../providers/error.provider';
import { CoreProvider } from 'src/providers/core.provider';
import { WeatherModule } from 'src/providers/weather/weather.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ContainersModule,
    RouterModule,
    WeatherModule
  ],
  providers: [
    ErrorProvider,
    CoreProvider,

    ConfigurationResolve
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
