import { ErrorHandler, Injectable } from '@angular/core';
import { Logger } from 'pi-dashboard-shared';


@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    logger = new Logger('GlobalErrorHandler');
    handleError(error) {
        try {
            this.logger.error(error);
        } catch (exp) {
            console.error('error while logging', exp);
        }
        throw error;
    }
}
