import {NgModule} from '@angular/core';
import {
    MatButtonModule, MatCardModule, MatDialogModule,
    MatStepperModule, MatFormFieldModule, MatInputModule,
    MatListModule, MatRadioModule, MatSelectModule,
    MatIconModule, MatAutocompleteModule, MatCheckboxModule,
    MatProgressBarModule, MatToolbarModule
} from '@angular/material';

@NgModule({
    imports: [
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MatStepperModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatRadioModule,
        MatSelectModule,
        MatIconModule,
        MatAutocompleteModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatToolbarModule,
    ],
    exports: [
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MatStepperModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatRadioModule,
        MatSelectModule,
        MatIconModule,
        MatAutocompleteModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatToolbarModule,
    ],
})
export class MaterialModule { }
