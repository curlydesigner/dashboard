import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
@Component({
    selector: 'app-global-error-dialog',
    templateUrl: './global-error-dialog.component.html',
    styleUrls: ['./global-error-dialog.component.css'],
})
export class GlobalErrorDialogComponent {
    error: string;
    constructor(public dialogRef: MatDialogRef<GlobalErrorDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.error = data.error;
    }

    refresh() {
        window.location.reload();
    }
}
