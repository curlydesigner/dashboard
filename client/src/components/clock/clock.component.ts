import { Component } from '@angular/core';
import * as moment from 'moment-timezone';
import { Input } from '@angular/core';

@Component({
    selector: 'app-clock',
    templateUrl: './clock.component.html',
    styleUrls: ['./clock.component.css'],
})
export class ClockComponent {
    @Input() timeZone = 'America/Toronto'; // America/Toronto, America/Los_Angeles, Asia/Jerusalem moment.tz(now, "America/Los_Angeles")
    @Input() showSeconds = false;

    hours: string;
    minutes: string;
    seconds: string;
    date: string;
    isPointOn = true;

    constructor() {
        setInterval(this.update.bind(this), 1000);
        this.update();
    }

    update() {
        const now = moment.tz(new Date(), this.timeZone);
        this.hours = now.format('HH');
        this.minutes = now.format('mm');
        this.seconds = now.format('ss');
        this.date = now.format('dddd, MMMM D');
        this.isPointOn = !this.isPointOn || this.showSeconds;
    }

}
