import { ClockComponent } from './clock/clock.component';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { GlobalErrorDialogComponent } from './global-error/global-error-dialog.component';
import { MaterialModule } from '../app/material.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    MaterialModule,
  ],
  declarations: [
    GlobalErrorDialogComponent,
    ClockComponent
  ],
  exports: [
    GlobalErrorDialogComponent,
    ClockComponent,
  ],
  entryComponents: [
    GlobalErrorDialogComponent,
  ]
})
export class ComponentsModule { }
