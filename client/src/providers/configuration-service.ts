import { ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, flatMap, map, tap } from 'rxjs/internal/operators';
import { Screen, Logger } from 'pi-dashboard-shared';
import { DataService } from './data/data-service';


export interface JsonConfig {
    serverUrl?: string;
    screenId?: string;
}

export interface LocalStorageConfig extends JsonConfig {
    screen: Screen;
}


export class ConfigurationService {
    static logger = new Logger('ConfigurationService');

    STORAGE_CONFIG_KEY = 'config';
    serverUrl: string;
    screen: Screen;
    screen$ = new ReplaySubject<Screen>();

    constructor(private config: JsonConfig, private dataProvider: DataService) {
        this.serverUrl = config.serverUrl;
    }

    /**
     *  Loads configuration from json file
     * @param http
     */
    static loadConfig(http: HttpClient): Observable<JsonConfig> {
        const envName = environment.production ? 'prod' : 'dev';
        const jsonFile = `assets/config.${envName}.json`;
        return http
            .get<JsonConfig>(jsonFile)
            .pipe(
                catchError(error => {
                    ConfigurationService.logger.error('Error while loading config file', jsonFile, error);
                    return of({});
                })
            );
    }

    /**
     * Broadcast screen configuration and save to local storage
     * @param screen
     */
    public setScreen(screen: Screen): Screen {
        ConfigurationService.logger.debug('setScreen', screen);
        this.screen = screen;
        this.screen$.next(screen);
        localStorage.setItem(this.STORAGE_CONFIG_KEY, JSON.stringify(screen));
        return this.screen;
    }

    init(): Observable<Screen> {
        this.screen = this.loadLocalStorageConfig() || {} as Screen;
        this.screen._id = this.screen._id || this.config.screenId;
        ConfigurationService.logger.debug('init screen', this.screen);
        return of(this.screen)
            .pipe(
                flatMap(screen => {
                    if (this.serverUrl && !!this.screen._id && !this.hasConfiguration()) {
                        ConfigurationService.logger.debug('load configuration from server');
                        return this.dataProvider
                            .getScreen(this.screen._id);
                    } else if (this.hasConfiguration()) {
                        ConfigurationService.logger.debug('use exists configuration');
                        return of(screen);
                    } else {
                        ConfigurationService.logger.debug('no configuration');
                        return of(screen);
                    }
                }),
                tap(screen => this.setScreen(screen))
            );
    }

    hasConfiguration(): boolean {
        return !!this.screen._id && !!this.screen.weather && !!this.screen.backgroundUpdate;
    }

    hasServer(): boolean {
        return !!this.serverUrl;
    }

    /**
     * Updates screen on the server and process it locally
     * @param screen
     */
    saveScreen(screen: Screen): Observable<Screen> {
        ConfigurationService.logger.debug('saveScreen', screen);
        if (this.hasServer()) {
            return this.dataProvider
                .saveScreen(screen)
                .pipe(map(this.setScreen.bind(this)));
        } else {
            return of(this.setScreen.bind(this));
        }
    }

    removeScreen(id: string) {
        return this.dataProvider
            .removeScreen(id)
            .pipe(map(response => {
                this.removeLocalStorageConfig();
                return response;
            }));
    }

    private loadLocalStorageConfig(): Screen {
        const str = localStorage.getItem(this.STORAGE_CONFIG_KEY);
        return str ? JSON.parse(str) : {};
    }

    private removeLocalStorageConfig() {
        localStorage.removeItem(this.STORAGE_CONFIG_KEY);
    }
}
