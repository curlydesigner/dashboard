import { WebSocketSubjectConfig, WebSocketSubject } from 'rxjs/webSocket';
import { Subject, Observable, Observer, BehaviorSubject, interval } from 'rxjs';
import { share, distinctUntilChanged, takeWhile } from 'rxjs/operators';

export class ReconnectWebsocketSubject<T> extends Subject<T> {
    private reconnectionObservable: Observable<number>;
    private wsSubjectConfig: WebSocketSubjectConfig<T>;
    private socket: WebSocketSubject<any>;
    private connectionObserver: Observer<boolean>;
    public connectionStatus: Observable<boolean>;
    public isConnected: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        private url: string,
        private reconnectInterval: number = 5000,
        // private reconnectAttempts: number = 10
    ) {
        super();

        this.connectionStatus = new Observable<boolean>((observer) => {
            this.connectionObserver = observer;
        }).pipe(
            share(),
            distinctUntilChanged()
        );

        this.wsSubjectConfig = {
            url: url,
            closeObserver: {
                next: (e: CloseEvent) => {
                    this.socket = null;
                    this.connectionObserver.next(false);
                }
            },
            openObserver: {
                next: (e: Event) => {
                    this.connectionObserver.next(true);
                }
            }
        };
        this.connect();
        this.connectionStatus.subscribe((isConnected) => {
            this.isConnected.next(isConnected);
            if (!this.reconnectionObservable && typeof (isConnected) === 'boolean' && !isConnected) {
                this.reconnect();
            }
        });
    }

    connect(): void {
        this.socket = new WebSocketSubject(this.wsSubjectConfig);
        this.socket.subscribe(
            (m) => {
                this.next(m);
            },
            (error: Event) => {
                if (!this.socket) {
                    this.reconnect();
                }
            });
    }

    reconnect(): void {
        this.reconnectionObservable = interval(this.reconnectInterval)
            .pipe(
                takeWhile((v, index) => {
                    // return index < this.reconnectAttempts && !this.socket;
                    return !this.socket;
                })
            );
        this.reconnectionObservable.subscribe(
            () => {
                this.connect();
            },
            null,
            () => {
                this.reconnectionObservable = null;
                if (!this.socket) {
                    this.complete();
                    this.connectionObserver.complete();
                }
            });
    }

    send(data: any): void {
        this.socket.next(data);
    }
}

