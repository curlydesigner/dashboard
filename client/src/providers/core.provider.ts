import { Injectable, Inject, OnDestroy } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Screen, Logger, WsMessage, DHTSensorData, Weather } from 'pi-dashboard-shared';
import { ConfigurationService, JsonConfig } from './configuration-service';
import { HttpClient } from '@angular/common/http';
import { catchError, flatMap, map, shareReplay } from 'rxjs/operators';
import { DataService } from './data/data-service';
import { DOCUMENT } from '@angular/common';
import { WsDataService } from './data/ws-data-service';
import { weatherServices } from './weather/weather-service';

export interface Services {
    dataService: DataService;
    configService: ConfigurationService;
    wsDataService: WsDataService;
    weatherService: Weather.Service;
}

@Injectable()
export class CoreProvider implements OnDestroy {
    private screen$: Observable<Screen>;
    private services$: Observable<Services>;
    private subscriptions: any[] = [];

    constructor(
        http: HttpClient,
        @Inject(DOCUMENT) document: Document
    ) {

        this.services$ = ConfigurationService
            .loadConfig(http)
            .pipe(
                catchError(() => of({} as JsonConfig)),
                flatMap(config => this.initServices(config, http, document)),
                shareReplay()
            );
        this.screen$ = this.services$
            .pipe(
                flatMap(({ configService }: Services) => configService.screen$),
            );

    }

    private initServices(config: JsonConfig, httpClient: HttpClient, document: Document): Observable<Services> {
        let dataService: DataService;
        if (config.serverUrl) {
            dataService = new DataService(config.serverUrl, httpClient, document);
        }
        const configService = new ConfigurationService(config, dataService);
        return configService
            .init()
            .pipe(
                map(screen => {
                    let wsDataService: WsDataService;
                    if (dataService.endPoint && screen._id) {
                        wsDataService = new WsDataService(dataService.endPoint, screen._id);
                        this.subscribeScreenUpdates(configService, wsDataService);
                        this.subscribeWindowRefresh(wsDataService);
                        this.subscribeLogs(wsDataService);
                    }
                    let weatherService: Weather.Service;
                    if (screen.weather) {
                        weatherService = new weatherServices[screen.weather.type](
                            httpClient,
                            Object.assign({}, screen.weather, { proxyUrl: `${config.serverUrl}/proxy` })
                        );
                    }
                    return {
                        dataService,
                        configService,
                        wsDataService,
                        weatherService
                    };
                })
            );
    }

    /**
     * Subscribes for screen configuration update messages
     * @param wsDataProvider
     */
    private subscribeScreenUpdates(configService: ConfigurationService, wsDataProvider: WsDataService) {
        const subscription = wsDataProvider
            .websocketMessages(WsMessage.Type.screen)
            .pipe(map(message => message.data))
            .subscribe(configService.setScreen.bind(configService));
        this.subscriptions.push(subscription);
    }

    private subscribeWindowRefresh(wsDataProvider: WsDataService) {
        const subscription = wsDataProvider
            .websocketMessages(WsMessage.Type.reload)
            .subscribe(() => {
                window.location.reload();
            });
        this.subscriptions.push(subscription);
    }

    private subscribeLogs(wsDataProvider: WsDataService) {
        const subscription = Logger.onLog.subscribe((log: Logger.Log) => {
            wsDataProvider.sendWebsocketMessage({ type: WsMessage.Type.log, data: log });
        });
        this.subscriptions.push(subscription);
    }

    getNextPhoto(clientId: string, backgroundUpdate: number, width: number, height: number): Observable<Blob> {
        return this.services$.pipe(
            flatMap(services => services.dataService.getNextPhoto(clientId, backgroundUpdate, width, height))
        );
    }

    getSensorData(): Observable<DHTSensorData> {
        return this.services$.pipe(flatMap(services => services.dataService.getSensorData()));
    }

    getScreen(): Observable<Screen> {
        return this.screen$;
    }

    getServices(): Observable<Services> {
        return this.services$;
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
}
