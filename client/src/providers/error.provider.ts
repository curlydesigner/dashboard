import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {GlobalErrorDialogComponent} from '../components/global-error/global-error-dialog.component';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class ErrorProvider {

    constructor(public dialog: MatDialog) {}

    global(error: string, refreshButton: boolean = false, disableClose: boolean = true): Observable<any> {
        let dialogRef = this.dialog.getDialogById('global-error');
        dialogRef = dialogRef ? dialogRef : this.dialog.open(GlobalErrorDialogComponent, {
            id: 'global-error',
            data: {
                error,
            },
            width: '30%',
            disableClose: disableClose,
            backdropClass: 'backdrop'
        });
        return dialogRef.afterClosed();
    }

    communication(): Observable<any> {
        const error = 'Client not able to connect with Server';
        this.global(error, true);
        return throwError(error);
    }
}
