import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from '../../app/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DarkSkyModule } from 'pi-dashboard-weather-dark-sky';
import { OpenWeatherModule } from 'pi-dashboard-weather-open-weather';

@NgModule({
    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        BrowserModule,
        MaterialModule,
        DarkSkyModule,
        OpenWeatherModule
    ]
})
export class WeatherModule { }

