import { Weather } from 'pi-dashboard-shared';
import { HttpClient } from '@angular/common/http';
import * as beaufort from 'beaufort-scale';
import { DarkSkyService, DarkSky } from 'pi-dashboard-weather-dark-sky';
import { OpenWeatherService, OpenWeather } from 'pi-dashboard-weather-open-weather';

export interface Beaufort {
    grade: number;
    desc: string;
}

export type WeatherServiceConstructor = new(http: HttpClient, config: Weather.Configuration) => Weather.Service;

export const weatherServices: {
    openWeather: WeatherServiceConstructor;
    darkSky: WeatherServiceConstructor;
} = {
    openWeather: OpenWeatherService,
    darkSky: DarkSkyService,
};

export const weatherConfigurationMetadata = [
    OpenWeather.configurationMetadata,
    DarkSky.configurationMetadata
];

const WIND_DIRECTIONS = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];

export function degreeToDirection(degree: number): string {
    const idx = ((degree / 22.5) + .5).toFixed(0);
    return WIND_DIRECTIONS[idx];
}

export interface Beaufort {
    grade: number; desc: string;
}

export function windSpeedToBeaufort(speed: number): Beaufort {
    return beaufort(speed);
}

export function fahrenheitToCelsius(fahrenheit: number): number {
    return (fahrenheit - 32) * 0.5556;
}


