import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Observable, interval, onErrorResumeNext, timer, EMPTY, NEVER, throwError, of } from 'rxjs';
import { repeatWhen, retryWhen, map, mergeMap, catchError, flatMap } from 'rxjs/operators';
import { Logger, Screen, DHTSensorData } from 'pi-dashboard-shared';

export class DataService {

    private static MAX_RETRY_ATTEMPTS = 3;
    private static retryAttempt = 0;

    endPoint: string;
    clientId: string;

    logger = new Logger('DataProvider');

    constructor(serverUrl, private http: HttpClient, @Inject(DOCUMENT) document) {
        this.endPoint = serverUrl;
        if (this.endPoint && !this.endPoint.startsWith('http')) {
            this.endPoint = document.baseURI + this.endPoint;
        }
    }

    getNextPhoto(clientId: string, backgroundUpdate: number, width: number, height: number): Observable<Blob> {
        const url = this.endPoint + '/photos/next/' + clientId + '?w=' + width + '&h=' + height;
        const options = { responseType: 'blob' as 'blob' };
        return this.http
            .get(url, options)
            .pipe(
                repeatWhen(() => interval(backgroundUpdate)),
                retryWhen((attempts$) => attempts$.pipe(
                    flatMap((error) => {
                        if (error.status === 0) {
                            return throwError(error);
                        }
                        DataService.retryAttempt++;
                        this.logger.error('Error while downloading photo, retry', DataService.retryAttempt, error);
                        return DataService.retryAttempt > DataService.MAX_RETRY_ATTEMPTS
                            ? EMPTY
                            : timer(DataService.retryAttempt * 1000);
                    })
                )),
                map(res => {
                    this.logger.debug('Photo downloaded');
                    DataService.retryAttempt = 0;
                    return res;
                })
            );
    }

    getDropBoxAuthenticationUrl(): Observable<string> {
        return this.http.get<string>(this.endPoint + '/dropbox-authentication-url');
    }

    saveScreen(screen: Screen): Observable<Screen> {
        if (screen._id) {
            return this.http.put<Screen>(this.endPoint + '/screens/' + screen._id, screen);
        } else {
            return this.http.post<Screen>(this.endPoint + '/screens', screen);
        }
    }

    getScreen(id: string): Observable<Screen> {
        return this.http.get<Screen>(this.endPoint + '/screens/' + id);
    }

    getScreens(): Observable<Screen[]> {
        return this.http.get<Screen[]>(this.endPoint + '/screens/');
    }

    removeScreen(id: string) {
        return this.http.delete(this.endPoint + '/screens/' + id);
    }

    getSensorData(): Observable<DHTSensorData> {
        return onErrorResumeNext(
            this.http
                .get<DHTSensorData>(this.endPoint + '/raspberry/get-sensor-data')
                .pipe(
                    repeatWhen(() => interval(1000 * 60)),
                    catchError(error => {
                        if (error.status === 0) {
                            return throwError(error);
                        } else {
                            this.logger.error('getSensorData', error);
                            return NEVER;
                        }
                    }),
                    map(data => {
                        this.logger.debug('indoor update', data);
                        return data;
                    })
                )
        );
    }
}
