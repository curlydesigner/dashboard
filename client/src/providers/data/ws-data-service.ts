import { of, Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Logger, WsMessage, WsClientType, WsClientUrlParam } from 'pi-dashboard-shared';
import { ReconnectWebsocketSubject } from '../reconnect-websocket-subject';

export class WsDataService {

    private socket$: ReconnectWebsocketSubject<WsMessage>;

    logger = new Logger('WsDataProvider');

    constructor(private serverUrl: string, screenId: string) {
        const wsEndpoint = this.serverUrl.replace('http://', 'ws://')
            + `/ws?${WsClientUrlParam.type}=${WsClientType.dashboard}&${WsClientUrlParam.id}=${screenId}`;
        this.socket$ = new ReconnectWebsocketSubject<WsMessage>(wsEndpoint);
    }

    websocketMessages(type: WsMessage.Type): Observable<WsMessage> {
        return this.socket$.pipe(
            map(message => {
                this.logger.debug('websocketMessages', message);
                return message;
            }),
            filter(message => message.type === type)
        );
    }

    sendWebsocketMessage(message: WsMessage): Observable<WsMessage> {
        this.socket$.send(message);
        return of(message);
    }
}
